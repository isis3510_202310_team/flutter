import 'package:flutter/material.dart';
import '../utils/sportify_colors.dart';

class MyTextField extends StatefulWidget {
  final controller;
  final String title;
  final String hintText;
  final bool obscureText;

  const MyTextField({
    Key? key,
    required this.controller,
    required this.title,
    required this.hintText,
    required this.obscureText,
  }) : super(key: key);

  @override
  _MyTextFieldState createState() => _MyTextFieldState();
}

class _MyTextFieldState extends State<MyTextField> {
  bool _showPassword = false;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 35.0, vertical: 5.0),
      child: Row(
        children: [
          Expanded(
            child: TextField(
              controller: widget.controller,
              obscureText: !_showPassword && widget.obscureText,
              decoration: InputDecoration(
                floatingLabelBehavior: FloatingLabelBehavior.always,
                labelText: widget.title,
                labelStyle: const TextStyle(
                  fontFamily: 'Roboto',
                  color: SportifyColors.primary,
                  fontSize: 20,
                  fontWeight: FontWeight.bold,
                ),
                hintText: widget.hintText,
                hintStyle: TextStyle(
                  fontFamily: 'Roboto',
                  color: Colors.grey[500],
                ),
                enabledBorder: const OutlineInputBorder(
                  borderSide: BorderSide(
                    color: SportifyColors.primary,
                    width: 2, // increase the width to make the border thicker
                  ),
                ),
                focusedBorder: const OutlineInputBorder(
                  borderSide: BorderSide(
                    color: SportifyColors.primary,
                    width: 2, // increase the width to make the border thicker
                  ),
                ),
                fillColor: SportifyColors.background,
                filled: true,
                suffixIcon: widget.obscureText
                    ? IconButton(
                  onPressed: () {
                    setState(() {
                      _showPassword = !_showPassword;
                    });
                  },
                  icon: Icon(
                    _showPassword ? Icons.visibility : Icons.visibility_off,
                    color: Colors.grey[500],
                  ),
                )
                    : null,
              ),
            ),
          ),
        ],
      ),
    );
  }
}