import 'dart:async';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import '../controllers/app_controller.dart';
import '../utils/sportify_colors.dart';

class ClockButton extends StatefulWidget {
  @override
  _ClockButtonState createState() => _ClockButtonState();
}

class _ClockButtonState extends State<ClockButton> {
  bool _isActive = false;
  AppController appController = AppController();

  void onButtonPressed() async{
    final ConnectivityResult connectivityResult = await appController.getConnectionStatus();
    setState(() {
      _isActive = !_isActive;
      if (_isActive) {
        if (connectivityResult == ConnectivityResult.none) {
          showDialog(
            context: context,
            builder: (context) =>
                AlertDialog(
                  backgroundColor: SportifyColors.quaternary,
                  title: Text(
                    'Notificación',
                    style: TextStyle(
                      color: SportifyColors.text,
                    ),
                  ),
                  content: Text(
                    'No tienes conexión a internet',
                    style: TextStyle(
                      color: SportifyColors.text,
                    ),
                  ),
                  actions: [
                    TextButton(
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                      child: Text(
                        'OK',
                        style: TextStyle(
                          color: SportifyColors.primary,
                        ),
                      ),
                    ),
                  ],
                ),
          );
        } else {
          //texto del tipo de conexion
          showDialog(
            context: context,
            builder: (context) =>
                AlertDialog(
                  backgroundColor: SportifyColors.quaternary,
                  title: Text(
                    'Notificación',
                    style: TextStyle(
                      color: SportifyColors.text,
                    ),
                  ),
                  content: Text(
                    'Tu conexión es: '+ connectivityResult.toString(),
                    style: TextStyle(
                      color: SportifyColors.text,
                    ),
                  ),
                  actions: [
                    TextButton(
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                      child: Text(
                        'OK',
                        style: TextStyle(
                          color: SportifyColors.primary,
                        ),
                      ),
                    ),
                  ],
                ),
          );
        }
      }
    });
  }



  @override
  Widget build(BuildContext context) {
    return IconButton(
      icon: Icon(_isActive ? Icons.access_time_filled : Icons.access_time),
      onPressed: () {
        onButtonPressed();
      },
    );
  }

  void getEvents(BuildContext context) async {
    final user = FirebaseAuth.instance.currentUser;
    final uid = user!.uid;
    final ref = FirebaseDatabase.instance.ref().child("events");
    ref.onValue.listen((event) {
      final Object? values = event.snapshot.value;
      if (values != null) {
        final Map<dynamic, dynamic> map = values as Map<dynamic, dynamic>;
        map.forEach((key, value) {
          if (value["idUser"] == uid) {
            DateTime now = DateTime.now();
            final format = DateFormat('h:mm a');
            final start = format.parse(value["startTime"]);
            final starthour = start.hour;
            final separar = value["date"].split("/");
            if (separar[0].length == 1) {
              separar[0] = "0" + separar[0];
            }
            if (separar[1].length == 1) {
              separar[1] = "0" + separar[1];
            }
            final parsedDate =
            DateTime.parse("${separar[2]}-${separar[1]}-${separar[0]}");
            final startdate = DateTime(parsedDate.year, parsedDate.month,
                parsedDate.day, starthour, start.minute);
            final formatnow = DateFormat('yyyy-MM-dd HH:mm').format(now);
            final onehour = startdate.subtract(Duration(hours: 1));
            final formatonehour = DateFormat('yyyy-MM-dd HH:mm').format(
                onehour);
            if (formatnow == formatonehour) {
              showNotification(context);
            }
          }
        });
      }
    });
  }

  Future<void> showNotification(BuildContext context) async {
    //modal de notificacion
    await showDialog(
      context: context,
      builder: (context) =>
          AlertDialog(
            backgroundColor: SportifyColors.quaternary,
            title: Text(
              'Notificación',
              style: TextStyle(
                color: SportifyColors.text,
              ),
            ),
            content: Text(
              'Tienes un evento en una hora',
              style: TextStyle(
                color: SportifyColors.text,
              ),
            ),
            actions: [
              TextButton(
                onPressed: () {
                  Navigator.of(context).pop();
                },
                child: Text(
                  'OK',
                  style: TextStyle(
                    color: SportifyColors.primary,
                  ),
                ),
              ),
            ],
          ),
    );
  }
}
