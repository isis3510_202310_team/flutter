import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter/material.dart';
import 'package:sportify/utils/sportify_colors.dart';
import 'package:sportify/views/home_page.dart';
import 'package:sportify/views/my_events.dart';
import '../controllers/app_controller.dart';
import '../views/createEvent/eventcreate1_page.dart';
import '../views/groups/group_list.dart';
import '../views/my_calendar.dart';
import '../views/profile_page.dart';
import 'dialogBox.dart';

class NavigationBarBottom extends StatelessWidget {

  AppController appController = AppController();

  void getPages(context, value) async {
    final connectivityResult = await appController.getConnectionStatus();
    if (value == 0) {
      Navigator.popAndPushNamed(context, '/home');
    } else if (value == 1) {
      Navigator.push(context,
          MaterialPageRoute(builder: (context) => const MyCalendarPage()));
    } else if (value == 2) {
      if (connectivityResult == ConnectivityResult.none) {
        showDialog(
          context: context,
          builder: (context) => DialogBox(
            title: 'Don\'t have internet connection',
            content: 'You need to have internet connection to create an event',
          ),
        );
      } else {
        Navigator.push(
            context, MaterialPageRoute(builder: (context) => CreateEvent1()));
      }
    } else if (value == 3) {
      if (connectivityResult == ConnectivityResult.none) {
        showDialog(
          context: context,
          builder: (context) => DialogBox(
            title: 'Don\'t have internet connection',
            content: 'You need to have internet connection to create an event',
          ),
        );
      } else{
      Navigator.push(
          context, MaterialPageRoute(builder: (context) => GroupList()));
      }
    } else if (value == 4) {
      Navigator.push(
          context, MaterialPageRoute(builder: (context) => const ProfilePage()));
    }
  }

  @override
  Widget build(BuildContext context) {
    return BottomNavigationBar(
      items: const [
        BottomNavigationBarItem(
          icon: Icon(Icons.home),
          label: 'Home',
        ),
        BottomNavigationBarItem(
          icon: Icon(Icons.calendar_month_rounded),
          label: 'Calendar',
        ),
        BottomNavigationBarItem(
          icon: Icon(Icons.add_box_outlined),
          label: 'Add',
        ),
        BottomNavigationBarItem(
          icon: Icon(Icons.people_alt_outlined),
          label: 'Groups',
        ),
        BottomNavigationBarItem(
          icon: Icon(Icons.account_circle_rounded),
          label: 'Profile',
        ),
      ],
      onTap: (value) => {
        getPages(context, value)
      },
      selectedItemColor: SportifyColors.primary,
      unselectedItemColor: SportifyColors.primary,
      type: BottomNavigationBarType.fixed,
      showUnselectedLabels: true,
      selectedFontSize: 14.0,
      unselectedFontSize: 14.0,
      selectedLabelStyle: TextStyle(fontWeight: FontWeight.bold),
      unselectedLabelStyle: TextStyle(fontWeight: FontWeight.normal),
    );
  }
}
