import 'package:flutter/material.dart';
import 'package:sportify/utils/sportify_colors.dart';

class DialogBox extends StatelessWidget {

  final String title;
  final String content;

  const DialogBox({
    Key? key, required this.title, required this.content,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      backgroundColor: SportifyColors.background,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(20.0),
      ),
      title: Text(
        title,
        style: TextStyle(
          color: SportifyColors.text,
        ),
        textAlign: TextAlign.center,
      ),
      content: Text(
        content,
        style: TextStyle(
          color: SportifyColors.text,
        ),
      ),
      actions: [
        TextButton(
          onPressed: () {
            Navigator.of(context).pop();
          },
          child: Text(
            'OK',
            style: TextStyle(
              color: SportifyColors.primary,
            ),
          ),
        ),
      ],
    );
  }
}