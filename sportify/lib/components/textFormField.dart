import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import '../utils/sportify_colors.dart';

class SportifyTextFormFieldDecorator extends StatelessWidget {
  final String hintText;
  final String? validationText;
  final IconData? iconData;
  final ValueChanged<String>? onChanged;
  final TextEditingController? controller;
  final BuildContext context;

  const SportifyTextFormFieldDecorator({
    required this.hintText,
    this.validationText,
    this.iconData,
    this.onChanged,
    this.controller,
    required this.context,
  });

  String? _validateInput(String? value) {
    if (value == null || value.isEmpty) {
        ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(
            content: Text(
              validationText!,
              style: TextStyle(
                fontFamily: 'Roboto',
                color: Colors.white,
                fontSize: 20,
              ),
            ),
            backgroundColor: SportifyColors.primary,
          ),
        );
        throw Exception('Please enter a valid input');
    }
    // Realice cualquier validación adicional que desee aquí.
    return null; // Devuelve nulo si la validación es exitosa
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: SportifyColors.quaternary,
        borderRadius: BorderRadius.circular(10),
      ),
      child: TextFormField(
        controller: controller,
        maxLines: null,
        style: TextStyle(
          color: SportifyColors.text,
        ),
        decoration: InputDecoration(
          border: InputBorder.none,
          hintText: hintText,
          contentPadding: const EdgeInsets.fromLTRB(10, 0, 0, 0),
          hintStyle: TextStyle(
            fontFamily: 'Roboto',
            color: Colors.grey[500],
          ),
        ),
        onChanged: onChanged,
        validator: _validateInput, // Agrega el validador personalizado
      ),
    );
  }
}