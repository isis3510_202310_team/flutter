import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';
import 'package:sportify/controllers/user_controller.dart';
import 'package:sportify/models/sportify_user.dart';
import '../controllers/app_controller.dart';
import '../models/authentication/auth_manager.dart';
import '../models/authentication/strategies/email_password_auth.dart';
import '../models/authentication/strategies/google_auth.dart';
import '../utils/sportify_colors.dart';

class ProfilePage extends StatefulWidget {
  const ProfilePage({Key? key}) : super(key: key);

  @override
  State<ProfilePage> createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> {

  late String? imageUrl;
  late String? userId;
  late String? authStrategy;
  late Image image = Image.asset('assets/images/logo.png');

  late EmailPasswordAuth emailPasswordAuth = EmailPasswordAuth();
  late GoogleAuth googleAuth = GoogleAuth();

  late AuthManager authManager;

  late DatabaseReference _userRef;

  final appController = AppController();
  final userController = UserController();

  SportifyUser? user;

  @override
  void initState() {
    super.initState();
    imageUrl = '';
    getImage();
    getAuthStrategy();
    getUser();
  }

  void getImage() async {
    appController.getImageUrl().then((value) => setState(() {
      imageUrl = value;

      appController.getImageFromCache(imageUrl!).then((value) => setState(() {
        if (value != null) {
          image = value;
        } else {
          appController.getImageFromFirebaseStorage().then((value) => setState(() {
            image = value;
          }));
        }
      }));
    }));
  }


  void getAuthStrategy() {
    appController.getAuthStrategy().then((value) => setState(() {
      authStrategy = value;
      if (authStrategy == 'password') {
        authManager = AuthManager(emailPasswordAuth);
      } else if (authStrategy == 'google.com') {
        authManager = AuthManager(googleAuth);
      }
    }));
  }

  void getUser() async {
    user = await userController.getUser();
    setState(() {});
  }

  void logOut() {
    authManager.logout();
    Navigator.pop(context);
    Navigator.popAndPushNamed(context, '/auth');
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Profile'),
        backgroundColor: SportifyColors.primary,
        actions: [
          IconButton(onPressed: logOut, icon: const Icon(Icons.logout))
        ],
      ),
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Align(
              alignment: Alignment.topCenter,
              child: Center(
                child: Column(
                  children: [
                    Container(
                      decoration: BoxDecoration(
                      shape: BoxShape.circle,
                        border: Border.all(
                          color: SportifyColors.primary,
                          width: 1.0,
                        ),
                    ),
                    child:
                      CircleAvatar(
                        radius: 80,
                        backgroundColor: Colors.transparent,
                        child: ClipOval(
                          child: AspectRatio(
                            aspectRatio: 1,
                            child: FittedBox(
                              fit: BoxFit.cover,
                              child: image,
                            ),
                          ),
                        ),
                      ),
                    ),
                    const SizedBox(height: 16),
                    Center(
                      child: Text(
                        user?.fullName ?? '',
                        style: const TextStyle(
                          fontSize: 28,
                          fontWeight: FontWeight.bold,
                          color: SportifyColors.primary
                        ),
                      ),
                    ),
                    Center(
                      child: Text(
                        user?.bio ?? '',
                        style: const TextStyle(
                          fontSize: 18,
                          fontWeight: FontWeight.normal,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
            const SizedBox(height: 16),
            Card(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const Padding(
                    padding: EdgeInsets.all(8.0),
                    child: Text(
                      'Birthdate',
                      style: TextStyle(
                        fontSize: 20,
                        fontWeight: FontWeight.bold,
                        color: SportifyColors.primary
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text(
                      user?.birthDate ?? '',
                      style: const TextStyle(
                        fontSize: 16,
                      ),
                    ),
                  ),
                ],
              ),
            ),
            const SizedBox(height: 16),
            Card(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const Padding(
                    padding: EdgeInsets.all(8.0),
                    child: Text(
                      'Email',
                      style: TextStyle(
                        fontSize: 20,
                        fontWeight: FontWeight.bold,
                        color: SportifyColors.primary
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text(
                      user?.email ?? '',
                      style: const TextStyle(
                        fontSize: 16,
                      ),
                    ),
                  ),
                ],
              ),
            ),
            const SizedBox(height: 16),
            Card(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const Padding(
                    padding: EdgeInsets.all(8.0),
                    child: Text(
                      'Phone Number',
                      style: TextStyle(
                        fontSize: 20,
                        fontWeight: FontWeight.bold,
                        color: SportifyColors.primary
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text(
                      user?.phone.toString() ?? '',
                      style: const TextStyle(
                        fontSize: 16,
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }




}
