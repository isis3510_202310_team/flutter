import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter/material.dart';
import 'package:sportify/controllers/app_controller.dart';
import 'package:sportify/controllers/notifications/notification_controller.dart';
import '../../controllers/createEvent/eventController.dart';
import '../../utils/sportify_colors.dart';
import 'package:firebase_database/firebase_database.dart';
import 'dart:io';
import 'package:image_picker/image_picker.dart';
import 'package:sportify/models/createEvent/event.dart';
import '../../components/textFormField.dart';
import 'package:sqflite/sqflite.dart';
import 'package:image_gallery_saver/image_gallery_saver.dart';


class CreateEvent4 extends StatefulWidget {
  final Event event;

  CreateEvent4({Key? key, required this.event}) : super(key: key);

  @override
  _CreateEvent4State createState() => _CreateEvent4State();
}

class _CreateEvent4State extends State<CreateEvent4> {
  final _textController = TextEditingController();
  String description = '';
  File? _imageFile;
  String? imagePath;
  EventController _eventController = EventController();
  AppController appController = AppController();
  NotificationController _notificationController = NotificationController();

  @override
  void dispose() {
    _textController.dispose();
    super.dispose();
  }

  void _saveImage() async {
    final String fileName = widget.event.name! + '.jpg';
    final String filePath = _imageFile!.path;
    final result = await ImageGallerySaver.saveFile(filePath, name: fileName);
    if (result['isSuccess']) {
      imagePath = filePath;
    }
  }

  void _selectImage() async {
    final ImagePicker _picker = ImagePicker();
    final XFile? pickedFile = await _picker.pickImage(
      source: ImageSource.gallery,
      maxWidth: 800,
      maxHeight: 800,
      imageQuality: 80,
    );
    if (pickedFile != null) {
      setState(() {
        _imageFile = File(pickedFile.path);
        imagePath = pickedFile.path;
      });
    }
  }

  void _takePicture() async {
    final ImagePicker _picker = ImagePicker();
    final XFile? pickedFile = await _picker.pickImage(
      source: ImageSource.camera,
      maxWidth: 800,
      maxHeight: 800,
      imageQuality: 80,
    );
    if (pickedFile != null) {
      setState(() {
        _imageFile = File(pickedFile.path);
        _saveImage();
      });
    }
  }

  void OnCreateEvent() async {
    widget.event.description = description;
    final connectivityResult = await appController.getConnectionStatus();
    if (connectivityResult == ConnectivityResult.none) {
      if (imagePath != null) {
        widget.event.image = imagePath;
      }else{
        widget.event.image = "";
      }
      Database database = await openDatabase(
        'temporal_events_database.db',
        version: 1,
        onCreate: (Database db, int version) async {
          await db.execute('CREATE TABLE temporal_events ('
              'id INTEGER PRIMARY KEY,'
              'idUser TEXT,'
              'name TEXT,'
              'address TEXT,'
              'sports TEXT,'
              'date TEXT,'
              'startTime TEXT,'
              'endTime TEXT,'
              'description TEXT,'
              'image TEXT'
              ')');
        },
      );
      int id = await database.insert(
        'temporal_events',
        widget.event.toDB(),
        conflictAlgorithm: ConflictAlgorithm.replace,
      );
      _eventController.uploadEvent(id, database);
      _notificationController.showNotification("Connection error", "Waiting for connection");
      Navigator.pushNamedAndRemoveUntil(context, '/home', (route) => false);
    } else {
      if (_imageFile == null) {
        widget.event.image = "";
      }else {
        final imageUrl = await _eventController.uploadImage(_imageFile!);
        widget.event.image = imageUrl;
      }
      final databaseReference = FirebaseDatabase.instance.ref();
      databaseReference
          .child('events')
          .push()
          .set(widget.event.toJson());
      Navigator.pushNamedAndRemoveUntil(context, '/home', (route) => false);
      _notificationController.showNotification("Success", "Event created successfully");
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Scaffold(
        backgroundColor: SportifyColors.background,
        appBar: AppBar(
          backgroundColor: SportifyColors.background,
          iconTheme: IconThemeData(
            color: SportifyColors.primary,
          ),
          leading: IconButton(
            icon: Icon(
              Icons.arrow_back,
              color: SportifyColors.primary,
            ),
            onPressed: () {
              Navigator.pop(context);
            },
          ),
        ),
        body: Padding(
          padding: EdgeInsets.all(16),
          child: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Padding(
                  padding: const EdgeInsets.only(top: 16.0, bottom: 16.0),
                  child: Text(
                    'Add an image for your event',
                    style: TextStyle(
                      fontFamily: 'Roboto',
                      color: SportifyColors.text,
                      fontSize: 25,
                      fontWeight: FontWeight.bold,
                    ),
                    textAlign: TextAlign.center,
                  ),
                ),
                InkWell(
                  onTap: () {},
                  child: Stack(
                    alignment: AlignmentDirectional.bottomEnd,
                    children: [
                      //avatar rectangular
                      Container(
                        width: 200,
                        height: 150,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(8),
                          color: SportifyColors.primary,
                        ),
                        child: _imageFile != null
                            ? ClipRRect(
                                borderRadius: BorderRadius.circular(8),
                                child: Image.file(
                                  _imageFile!,
                                  fit: BoxFit.cover,
                                ),
                              )
                            : const Icon(
                                Icons.add_a_photo,
                                color: Colors.white,
                                size: 50,
                              ),
                      ),
                      Container(
                        margin: const EdgeInsets.only(right: 8, bottom: 8),
                        width: 25,
                        height: 25,
                        decoration: const ShapeDecoration(
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.all(Radius.circular(8)),
                          ),
                          color: SportifyColors.primary,
                        ),
                        child: IconButton(
                            onPressed: () {
                              showModalBottomSheet(
                                context: context,
                                builder: (context) {
                                  return Container(
                                    height: 120,
                                    child: Column(
                                      children: <Widget>[
                                        ListTile(
                                          leading:
                                              const Icon(Icons.photo_library),
                                          title: const Text('Photo Library'),
                                          onTap: () {
                                            Navigator.pop(context);
                                            _selectImage();
                                          },
                                        ),
                                        ListTile(
                                          leading:
                                              const Icon(Icons.photo_camera),
                                          title: const Text('Camera'),
                                          onTap: () {
                                            Navigator.pop(context);
                                            _takePicture();
                                          },
                                        ),
                                      ],
                                    ),
                                  );
                                },
                              );
                            },
                            iconSize: 18,
                            padding: const EdgeInsets.all(1),
                            icon: const Icon(Icons.add, color: Colors.white)),
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 16.0, bottom: 16.0),
                  child: Text(
                    'Do you want to add information?',
                    style: TextStyle(
                      fontFamily: 'Roboto',
                      color: SportifyColors.text,
                      fontSize: 25,
                      fontWeight: FontWeight.bold,
                    ),
                    textAlign: TextAlign.center,
                  ),
                ),
                Container(
                  width: double.infinity,
                  height: 200,
                  padding: EdgeInsets.symmetric(horizontal: 16),
                  child: SportifyTextFormFieldDecorator(
                    context: context,
                    hintText: 'Enter additional information',
                    onChanged: (value) {
                      setState(() {
                        description = value;
                      });
                    },
                  ),
                ),
              ],
            ),
          ),
        ),
        bottomNavigationBar: BottomAppBar(
          elevation: 0,
          color: Colors.transparent,
          height: 100,
          child: Center(
            child: Container(
              constraints: BoxConstraints(maxWidth: 200),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(35),
                color: SportifyColors.primary,
              ),
              child: TextButton(
                  onPressed: () {
                    OnCreateEvent();
                  },
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      SizedBox(width: 8),
                      Text(
                        'Create Event',
                        style: TextStyle(
                          fontFamily: 'Roboto',
                          fontSize: 20,
                          fontWeight: FontWeight.bold,
                          color: SportifyColors.background,
                        ),
                      ),
                    ],
                  )),
            ),
          ),
        ),
      ),
    );
  }
}
