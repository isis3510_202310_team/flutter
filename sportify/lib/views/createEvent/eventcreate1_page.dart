import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:google_maps_place_picker_mb/google_maps_place_picker.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:sportify/controllers/googleAPI/locationController.dart';
import '../../controllers/app_controller.dart';
import '../../models/createEvent/event.dart';
import '../../models/googleAPI/locationModel.dart';
import 'eventcreate2_page.dart';
import '../../components/textFormField.dart';
import '../../utils/sportify_colors.dart';

class CreateEvent1 extends StatefulWidget {
  @override
  _CreateEvent1State createState() => _CreateEvent1State();
}

class _CreateEvent1State extends State<CreateEvent1> {
  String _EventName = '';
  final _formKey = GlobalKey<FormState>();
  final locationController = LocationController(LocationModel());
  AppController appController = AppController();

  void onPlacePicked(result) {
    setState(() {
      locationController.onPlacePicked(result);
    });
    Navigator.of(context).pop();
  }

  void onMarkerTapped(LatLng location) {
    setState(() {
      locationController.onMarkerTapped(location);
    });
  }

  Future<void> _selectPlace(BuildContext context) async {
    final connectivityResult = await appController.getConnectionStatus();
    if (connectivityResult == ConnectivityResult.none) {
      showDialog(
        context: context,
        builder: (context) =>
            AlertDialog(
              backgroundColor: SportifyColors.quaternary,
              title: Text(
                'Don\'t have internet connection',
                style: TextStyle(
                  color: SportifyColors.text,
                ),
              ),
              content: Text(
                'You need internet connection to create an event',
                style: TextStyle(
                  color: SportifyColors.text,
                ),
              ),
              actions: [
                TextButton(
                  onPressed: () {
                    Navigator.pushNamedAndRemoveUntil(context, '/home', (route) => false);
                  },
                  child: Text(
                    'OK',
                    style: TextStyle(
                      color: SportifyColors.primary,
                    ),
                  ),
                ),
              ],
            ),
      );
    } else {
      await Navigator.of(context).push(MaterialPageRoute(
        builder: (context) {
          return PlacePicker(
            apiKey: locationController.locationModel.apiKey,
            initialPosition: locationController.locationModel.initialLocation,
            useCurrentLocation: true,
            selectInitialPosition: true,
            onPlacePicked: onPlacePicked,
          );
        },
      ));
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: SportifyColors.background,
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: SportifyColors.background,
          iconTheme: IconThemeData(
            color: SportifyColors.primary,
          ),
        ),
        body: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Padding(
                padding: const EdgeInsets.only(top: 16.0),
                child: Text(
                  'What is the name of your event?',
                  style: TextStyle(
                    fontFamily: 'Roboto',
                    color: SportifyColors.text,
                    fontSize: 25,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
              Form(
                key: _formKey,
                child: Padding(
                  padding:
                      const EdgeInsets.only(top: 16.0, left: 16.0, right: 16.0),
                  child: SportifyTextFormFieldDecorator(
                    context: context,
                    hintText: 'Event name',
                    validationText: 'Please enter a name for your event',
                    onChanged: (value) {
                      setState(() {
                        _EventName = value;
                      });
                    },
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 32.0, bottom: 16.0),
                child: Text(
                  'Where does the event take place?',
                  style: TextStyle(
                    fontFamily: 'Roboto',
                    color: SportifyColors.text,
                    fontSize: 25,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
              Visibility(
                visible:
                    locationController.locationModel.selectedLocation != null,
                child: Container(
                  margin: EdgeInsets.symmetric(horizontal: 16.0),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10.0),
                    color: SportifyColors.quinary,
                  ),
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Center(
                      child: FutureBuilder<String>(
                        future: locationController.getLocationName(
                            locationController
                                    .locationModel.selectedLocation?.latitude ??
                                0.0,
                            locationController.locationModel.selectedLocation
                                    ?.longitude ??
                                0.0),
                        builder: (BuildContext context,
                            AsyncSnapshot<String> snapshot) {
                          if (snapshot.hasData) {
                            return Text(
                              '${snapshot.data}',
                              style: TextStyle(
                                fontFamily: 'Roboto',
                                fontSize: 16.0,
                                color: SportifyColors.text,
                              ),
                              textAlign: TextAlign.center,
                            );
                          } else {
                            return CircularProgressIndicator();
                          }
                        },
                      ),
                    ),
                  ),
                ),
              ),
              Container(
                padding: EdgeInsets.all(16.0),
                height: 300,
                width: double.infinity,
                child: GoogleMap(
                  initialCameraPosition: CameraPosition(
                    target: locationController.locationModel.initialLocation,
                    zoom: 14.0,
                  ),
                  onMapCreated: locationController.onMapCreated,
                  markers: locationController.locationModel.markers,
                  onTap: onMarkerTapped,
                ),
              ),
              Container(
                margin: EdgeInsets.only(left: 16.0, right: 16.0),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10.0),
                  color: SportifyColors.quinary,
                ),
                child: TextButton(
                  onPressed: () => _selectPlace(context),
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Icon(
                          Icons.place,
                          color: SportifyColors.primary,
                        ),
                        SizedBox(width: 8.0),
                        Text(
                          'Select Location',
                          style: TextStyle(
                            fontFamily: 'Roboto',
                            fontSize: 16.0,
                            color: SportifyColors.primary,
                          ),
                        ),
                      ],
                    ),
                  ),
                  style: TextButton.styleFrom(
                    foregroundColor: SportifyColors.primary,
                  ),
                ),
              ),
            ],
          ),
        ),
        bottomNavigationBar: BottomAppBar(
          elevation: 0,
          color: Colors.transparent,
          height: 100,
          child: Center(
            child: Container(
              constraints: BoxConstraints(maxWidth: 200),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(35),
                color: SportifyColors.quaternary,
              ),
              child: TextButton(
                onPressed: () {
                  if (_formKey.currentState!.validate()) {
                    if (locationController.locationModel.selectedLocation ==
                        null) {
                      ScaffoldMessenger.of(context).showSnackBar(
                        SnackBar(
                          content: Text(
                            'Please select a location',
                            style: TextStyle(
                              fontFamily: 'Roboto',
                              color: Colors.white,
                              fontSize: 20,
                            ),
                          ),
                          backgroundColor: SportifyColors.primary,
                        ),
                      );
                      return;
                    } else {
                      final user = FirebaseAuth.instance.currentUser;
                      final uid = user!.uid;
                      Event event = Event(
                        idUser: uid,
                        name: _EventName,
                        address: locationController.locationModel.address,
                      );
                      Navigator.of(context).push(
                        MaterialPageRoute(
                            builder: (_) => CreateEvent2(
                                  event: event,
                                )),
                      );
                    }
                  }
                },
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Icon(
                      Icons.arrow_forward,
                      color: SportifyColors.primary,
                    ),
                    SizedBox(width: 8),
                    Text(
                      'Next',
                      style: TextStyle(
                        fontFamily: 'Roboto',
                        fontSize: 20,
                        fontWeight: FontWeight.bold,
                        color: SportifyColors.primary,
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
