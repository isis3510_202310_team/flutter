import 'package:flutter/material.dart';
import 'package:sportify/models/createEvent/event.dart';
import 'package:table_calendar/table_calendar.dart';
import '../../utils/sportify_colors.dart';
import 'eventcreate4_page.dart';

class CreateEvent3 extends StatefulWidget {
  final Event event;

  CreateEvent3({Key? key, required this.event})
      : super(key: key);
  @override
  _CreateEvent3State createState() => _CreateEvent3State();
}

class _CreateEvent3State extends State<CreateEvent3> {
  CalendarFormat _calendarFormat = CalendarFormat.month;
  DateTime _focusedDay = DateTime.now();
  DateTime _selectedDay = DateTime.now();
  TimeOfDay? _startTime;
  TimeOfDay? _endTime;

  Future<void> _selectStartTime(BuildContext context) async {
    final ThemeData theme = Theme.of(context);
    final ColorScheme colorScheme = theme.colorScheme;

    final TimeOfDay? newTime = await showTimePicker(
      context: context,
      initialTime: TimeOfDay.now(),
      builder: (BuildContext context, Widget? child) {
        return Theme(
          data: ThemeData(
            colorScheme: colorScheme.copyWith(
              primary: SportifyColors.primary,
              onPrimary: SportifyColors.background,
              surface: SportifyColors.quinary,
              onSurface: SportifyColors.text,
            ),
          ),
          child: child!,
        );
      },
    );
    if (newTime != null) {
      setState(() {
        _startTime = newTime;
      });
    }
  }

  Future<void> _selectEndTime(BuildContext context) async {
    final ThemeData theme = Theme.of(context);
    final ColorScheme colorScheme = theme.colorScheme;

    final TimeOfDay? newTime = await showTimePicker(
      context: context,
      initialTime: TimeOfDay.now(),
      builder: (BuildContext context, Widget? child) {
        return Theme(
          data: ThemeData(
            colorScheme: colorScheme.copyWith(
              primary: SportifyColors.primary,
              onPrimary: SportifyColors.background,
              surface: SportifyColors.quinary,
              onSurface: SportifyColors.text,
            ),
          ),
          child: child!,
        );
      },
    );
    if (newTime != null) {
      setState(() {
        _endTime = newTime;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Scaffold(
        backgroundColor: SportifyColors.background,
        appBar: AppBar(
          backgroundColor: SportifyColors.background,
          iconTheme: IconThemeData(
            color: SportifyColors.primary,
          ),
          leading: IconButton(
            icon: Icon(
              Icons.arrow_back,
              color: SportifyColors.primary,
            ),
            onPressed: () {
              Navigator.pop(context);
            },
          ),
        ),
        body: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.all(16),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Padding(
                padding: const EdgeInsets.only(top: 16.0, bottom: 16.0),
                child: Text(
                  'When is the event?',
                  style: TextStyle(
                    fontFamily: 'Roboto',
                    color: SportifyColors.text,
                    fontSize: 25,
                    fontWeight: FontWeight.bold,
                  ),
                  textAlign: TextAlign.center,
                ),
              ),
              Container(
                margin: EdgeInsets.only(bottom: 16),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(20),
                  color: SportifyColors.quaternary,
                ),
                child: TableCalendar(
                  headerStyle: HeaderStyle(
                    titleTextStyle: TextStyle(
                        fontSize: 20,
                        fontWeight: FontWeight.bold,
                        fontFamily: 'Roboto',
                        color: SportifyColors.text),
                    formatButtonVisible: false,
                  ),
                  daysOfWeekStyle: DaysOfWeekStyle(
                    weekdayStyle: TextStyle(
                        fontSize: 16,
                        fontFamily: 'Roboto',
                        color: SportifyColors.text),
                    weekendStyle: TextStyle(
                        fontSize: 16,
                        fontFamily: 'Roboto',
                        color: SportifyColors.text),
                  ),
                  calendarStyle: CalendarStyle(
                    selectedDecoration: BoxDecoration(
                      color: SportifyColors.primary,
                      shape: BoxShape.circle,
                    ),
                    selectedTextStyle: TextStyle(color: Colors.white),
                    todayDecoration: BoxDecoration(
                      color: SportifyColors.secondary,
                      shape: BoxShape.circle,
                    ),
                    todayTextStyle: TextStyle(color: Colors.white),
                    outsideTextStyle: TextStyle(color: Colors.grey),
                    outsideDaysVisible: false,
                  ),
                  locale: 'en_US',
                  firstDay: DateTime.utc(2010, 10, 16),
                  lastDay: DateTime.utc(2030, 3, 14),
                  focusedDay: _focusedDay,
                  calendarFormat: _calendarFormat,
                  selectedDayPredicate: (day) {
                    // Use `selectedDayPredicate` to determine which day is currently selected.
                    // If this returns true, then `day` will be marked as selected.

                    // Using `isSameDay` is recommended to disregard
                    // the time-part of compared DateTime objects.
                    return isSameDay(_selectedDay, day);
                  },
                  onDaySelected: (selectedDay, focusedDay) {
                    if (!isSameDay(_selectedDay, selectedDay)) {
                      // Call `setState()` when updating the selected day
                      setState(() {
                        _selectedDay = selectedDay;
                        _focusedDay = focusedDay;
                      });
                    }
                  },
                  onFormatChanged: (format) {
                    if (_calendarFormat != format) {
                      // Call `setState()` when updating calendar format
                      setState(() {
                        _calendarFormat = format;
                      });
                    }
                  },
                  onPageChanged: (focusedDay) {
                    // No need to call `setState()` here
                    _focusedDay = focusedDay;
                  },
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(bottom: 16.0),
                child: Text(
                  'At what time is it?',
                  style: TextStyle(
                    fontFamily: 'Roboto',
                    color: SportifyColors.text,
                    fontSize: 25,
                    fontWeight: FontWeight.bold,
                  ),
                  textAlign: TextAlign.center,
                ),
              ),
              Container(
                width: 250,
                height: 80,
                padding: EdgeInsets.all(10),
                decoration: BoxDecoration(
                  color: SportifyColors.quaternary,
                  borderRadius: BorderRadius.circular(20),
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    ElevatedButton(
                      onPressed: () => _selectStartTime(context),
                      style: ButtonStyle(
                        backgroundColor: MaterialStateProperty.all<Color>(
                            SportifyColors.quaternary),
                        shape:
                            MaterialStateProperty.all<RoundedRectangleBorder>(
                          RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(30),
                            side: BorderSide(
                              color: SportifyColors.primary,
                              width: 2,
                            ),
                          ),
                        ),
                      ),
                      child: Text(
                        _startTime?.format(context) ?? 'Start Time',
                        style: TextStyle(
                          color: SportifyColors.text,
                        ),
                      ),
                    ),
                    SizedBox(
                      width: 40,
                      child: Icon(
                        Icons.arrow_forward,
                        color: SportifyColors.primary,
                      ),
                    ),
                    ElevatedButton(
                      onPressed: () => _selectEndTime(context),
                      style: ButtonStyle(
                        backgroundColor: MaterialStateProperty.all<Color>(
                            SportifyColors.quaternary),
                        shape:
                            MaterialStateProperty.all<RoundedRectangleBorder>(
                          RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(30),
                            side: BorderSide(
                              color: SportifyColors.primary,
                              width: 2,
                            ),
                          ),
                        ),
                      ),
                      child: Text(
                        _endTime?.format(context) ?? 'End Time',
                        style: TextStyle(
                          color: SportifyColors.text,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        )
                ),
        bottomNavigationBar: BottomAppBar(
          elevation: 0,
          color: Colors.transparent,
          height: 100,
          child: Center(
            child: Container(
              constraints: BoxConstraints(maxWidth: 200),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(35),
                color: SportifyColors.quaternary,
              ),
              child: TextButton(
                  onPressed: () {
                    if (_selectedDay.isBefore(DateTime.now())) {
                    ScaffoldMessenger.of(context).showSnackBar(
                    SnackBar(
                    content: Text('The date must be after today', style: TextStyle(
                      fontFamily: 'Roboto',
                      color: Colors.white,
                      fontSize: 20,
                    ),),
                    backgroundColor: SportifyColors.primary,
                    ),
                    );
                    }
                    else if (_startTime == null || _endTime == null) {
                    ScaffoldMessenger.of(context).showSnackBar(

                    SnackBar(
                    content: Text('You must select a time',
                      style: TextStyle(
                        fontFamily: 'Roboto',
                        color: Colors.white,
                        fontSize: 20,
                      ),),
                    backgroundColor: SportifyColors.primary,

                    ),
                    );
                    }
                    //revisar que la hora de inicio sea menor que la hora de fin
                    else if (_startTime!.hour >= _endTime!.hour) {
                      ScaffoldMessenger.of(context).showSnackBar(
                        SnackBar(
                          content: Text(
                              'The start time must be before the end time', style: TextStyle(
                            fontFamily: 'Roboto',
                            color: Colors.white,
                            fontSize: 20,
                          ),),
                          backgroundColor: SportifyColors.primary,
                        ),
                      );
                    }
                    else{
                      var date = _selectedDay.day.toString() +
                          '/' +
                          _selectedDay.month.toString() +
                          '/' +
                          _selectedDay.year.toString();
                      var startTime = _startTime?.format(context) ?? '';
                      var endTime = _endTime?.format(context) ?? '';
                      widget.event.date = date;
                      widget.event.startTime = startTime;
                      widget.event.endTime = endTime;

                      Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => CreateEvent4(
                                  event: widget.event,
                                )),
                      );
                    }
                  },
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Icon(
                        Icons.arrow_forward,
                        color: SportifyColors.primary,
                      ),
                      SizedBox(width: 8),
                      Text(
                        'Next',
                        style: TextStyle(
                          fontFamily: 'Roboto',
                          fontSize: 20,
                          fontWeight: FontWeight.bold,
                          color: SportifyColors.primary,
                        ),
                      ),
                    ],
                  )),
            ),
          ),
        ),
      ),
    );
  }
}
