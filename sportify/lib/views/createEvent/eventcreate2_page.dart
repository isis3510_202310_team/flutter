import 'package:flutter/material.dart';
import 'package:sportify/controllers/createEvent/sportsController.dart';
import 'package:sportify/models/createEvent/event.dart';
import 'package:sportify/models/createEvent/sports.dart';
import '../../utils/sportify_colors.dart';
import 'eventcreate3_page.dart';

class CreateEvent2 extends StatefulWidget {
  final Event event;

  CreateEvent2({Key? key, required this.event})
      : super(key: key);

  @override
  _CreateEvent2State createState() => _CreateEvent2State();
}

class _CreateEvent2State extends State<CreateEvent2> {

  SportsController sportsController = SportsController(ListSports());
  final _sports = ListSports().getSports();

  final Map<String, bool> _selectedSports = {};

  @override
  void initState() {

    super.initState();
    _sports.forEach((sport) {
      _selectedSports[sport.name] = false;
    });
  }

  Widget _buildSportButton(Map<String, String> sport) {
    final isSelected = _selectedSports[sport['name']!]!;
    final color = isSelected ? SportifyColors.primary : Colors.grey;

    return ElevatedButton(
      style: ButtonStyle(
        elevation: MaterialStateProperty.all<double>(6),
        backgroundColor: MaterialStateProperty.all<Color>(color),
        shape: MaterialStateProperty.all<RoundedRectangleBorder>(
          RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(20),
          ),
        ),
      ),
      onPressed: () {
        setState(() {
          _selectedSports[sport['name']!] = !isSelected;
        });
      },
      child: Row(
        mainAxisSize: MainAxisSize.min,
        children: [
          Text(
            sport['emoji']!,
            style: TextStyle(fontSize: 20),
          ),
          SizedBox(width: 8),
          Text(
            sport['name']!,
            style: TextStyle(
              fontFamily: 'Roboto',
              color: Colors.white,
              fontSize: 20,
            ),
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Scaffold(
        backgroundColor: SportifyColors.background,
        appBar: AppBar(
          backgroundColor: SportifyColors.background,
          iconTheme: IconThemeData(
            color: SportifyColors.primary,
          ),
          leading: IconButton(
            icon: Icon(
              Icons.arrow_back,
              color: SportifyColors.primary,
            ),
            onPressed: () {
              Navigator.pop(context);
            },
          ),
        ),
        body: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Padding(
              padding: const EdgeInsets.only(top: 16.0, bottom: 16.0),
              child: Text(
                'Which sports do you want to include?',
                style: TextStyle(
                  fontFamily: 'Roboto',
                  color: SportifyColors.text,
                  fontSize: 25,
                  fontWeight: FontWeight.bold,
                ),
                textAlign: TextAlign.center,
              ),
            ),
            Wrap(
              runSpacing: 5,
              spacing: 5,
              alignment: WrapAlignment.center,
              children:
                  sportsController.getSportsMap().map((sport) => _buildSportButton(sport)).toList(),
            ),
          ],
        ),
        bottomNavigationBar: BottomAppBar(
          elevation: 0,
          color: Colors.transparent,
          height: 100,
          child: Center(
            child: Container(
              constraints: BoxConstraints(maxWidth: 200),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(35),
                color: SportifyColors.quaternary,
              ),
              child: TextButton(
                onPressed: () {
                  if (_selectedSports.values.contains(true)) {
                    var sports = _selectedSports.keys.where((sport) => _selectedSports[sport]!).toList();
                    widget.event.sports = sports;
                    Navigator.of(context).push(
                      MaterialPageRoute(builder: (_) => CreateEvent3(
                        event: widget.event,
                      )),
                    );
                  } else {
                    ScaffoldMessenger.of(context).showSnackBar(
                      SnackBar(
                        content: Text(
                          'Please select at least one sport',
                          style: TextStyle(
                            fontFamily: 'Roboto',
                            color: Colors.white,
                            fontSize: 20,
                          ),
                        ),
                        backgroundColor: SportifyColors.primary,
                      ),
                    );
                  }
                },
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Icon(
                      Icons.arrow_forward,
                      color: SportifyColors.primary,
                    ),
                    SizedBox(width: 8),
                    Text(
                      'Next',
                      style: TextStyle(
                        fontFamily: 'Roboto',
                        fontSize: 20,
                        fontWeight: FontWeight.bold,
                        color: SportifyColors.primary,
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
