import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:intl/intl.dart';
import 'package:table_calendar/table_calendar.dart';

import '../controllers/app_controller.dart';
import '../utils/sportify_colors.dart';
import 'package:sqflite/sqflite.dart';

class MyCalendarPage extends StatefulWidget {
  const MyCalendarPage({Key? key}) : super(key: key);

  @override
  State<MyCalendarPage> createState() => _MyCalendarPageState();
}

class _MyCalendarPageState extends State<MyCalendarPage> {
  List<dynamic> myEvents = [];
  List<dynamic> filteredEvents = [];

  AppController appController = AppController();

  CalendarFormat _calendarFormat = CalendarFormat.month;
  DateTime _focusedDay = DateTime.now();
  DateTime _selectedDay = DateTime.now();

  late Database? database;

  @override
  void initState() {
    super.initState();
    openDatabaseAndCreateTable().then((_) {
      getEvents(context);
    });
  }

  Future<void> openDatabaseAndCreateTable() async {
    final databasePath = await getDatabasesPath();
    final databasePathWithName = '$databasePath/events.db';

    database = await openDatabase(
      databasePathWithName,
      version: 1,
      onCreate: (db, version) {
        return db.execute(
          'CREATE TABLE IF NOT EXISTS events (id INTEGER PRIMARY KEY, name TEXT, date TEXT, address TEXT, description TEXT)',
        );
      },
    );
  }

  Future<void> loadEventsFromDatabase() async {
    final queryResult = await database?.query('events');

    if (queryResult != null) {
      final events = queryResult.map((row) {
        final dateFormat = DateFormat('dd/M/yyyy');
        final parsedDate = dateFormat.parse(row["date"].toString());

        return {
          'name': row['name'],
          'date': parsedDate,
          'address': row['address'],
          'description': row['description'],
        };

      }).toList();

      setState(() {
        myEvents = events;
        filteredEvents = events;
      });
    }
  }

  Future<void> updateEventsInDatabase(List<dynamic> events) async {
    await database?.delete('events');

    await database?.transaction((txn) async {
      for (final event in events) {
        final dateFormat = DateFormat('dd/M/yyyy');
        final formattedDate = dateFormat.format(event['date']);

        await txn.insert(
          'events',
          {
            'name': event['name'],
            'date': formattedDate,
            'address': event['address'],
            'description': event['description'],
          },
        );
      }
    });
  }


  void filterEvents(DateTime focusedDay) {
    final filtered = myEvents.where((event) => isSameDay(event['date'], focusedDay)).toList();
    setState(() {
      filteredEvents = filtered;
      _focusedDay = focusedDay;
    });
  }

  void getEvents(BuildContext context) async {
    final user = FirebaseAuth.instance.currentUser;
    final uid = user!.uid;
    final ref = FirebaseDatabase.instance.ref().child("events");

    final connectivityResult = await appController.getConnectionStatus();
    if (connectivityResult == ConnectivityResult.none) {
      await loadEventsFromDatabase();
    }

    else {
      ref.onValue.listen((event) {
        final Object? values = event.snapshot.value;
        if (values != null) {
          final Map<dynamic, dynamic> map = values as Map<dynamic, dynamic>;
          final List<dynamic> events = [];
          map.forEach((key, value) {
            if (value["idUser"] == uid) {
              final dateFormat = DateFormat('dd/M/yyyy');
              final parsedDate = dateFormat.parse(value["date"]);

              value['date'] = parsedDate;

              events.add(value);
            }
          });
          setState(() {
            updateEventsInDatabase(events);
            myEvents = events;
            filteredEvents = events;
          });
        }
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('My Calendar'),
        backgroundColor: SportifyColors.primary,
      ),
      body: Column(
        children: [
          Container(
            margin: const EdgeInsets.only(bottom: 16),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(20),
              color: SportifyColors.quaternary,
            ),
            child: TableCalendar(
              headerStyle: const HeaderStyle(
                titleTextStyle: TextStyle(
                    fontSize: 20,
                    fontWeight: FontWeight.bold,
                    fontFamily: 'Roboto',
                    color: SportifyColors.text),
                formatButtonVisible: false,
              ),
              daysOfWeekStyle: const DaysOfWeekStyle(
                weekdayStyle: TextStyle(
                    fontSize: 16,
                    fontFamily: 'Roboto',
                    color: SportifyColors.text),
                weekendStyle: TextStyle(
                    fontSize: 16,
                    fontFamily: 'Roboto',
                    color: SportifyColors.text),
              ),
              calendarStyle: const CalendarStyle(
                selectedDecoration: BoxDecoration(
                  color: SportifyColors.primary,
                  shape: BoxShape.circle,
                ),
                selectedTextStyle: TextStyle(color: Colors.white),
                todayDecoration: BoxDecoration(
                  color: SportifyColors.secondary,
                  shape: BoxShape.circle,
                ),
                todayTextStyle: TextStyle(color: Colors.white),
                outsideTextStyle: TextStyle(color: Colors.grey),
                outsideDaysVisible: false,
              ),
              locale: 'en_US',
              firstDay: DateTime.utc(2010, 10, 16),
              lastDay: DateTime.utc(2030, 3, 14),
              focusedDay: _focusedDay,
              calendarFormat: _calendarFormat,
              selectedDayPredicate: (day) {
                return isSameDay(_selectedDay, day);
              },
              onDaySelected: (selectedDay, focusedDay) {
                if (!isSameDay(_selectedDay, selectedDay)) {
                  setState(() {
                    _selectedDay = selectedDay;
                    _focusedDay = focusedDay;
                  });
                  filterEvents(focusedDay);
                }
              },
              onFormatChanged: (format) {
                if (_calendarFormat != format) {
                  setState(() {
                    _calendarFormat = format;
                  });
                }
              },
              onPageChanged: (focusedDay) {
                _focusedDay = focusedDay;
                filterEvents(focusedDay);
              },
            ),
          ),
          Row(
            children: [
              const Padding(
                padding: EdgeInsets.only(left: 16.0),
                child: Text(
                  'My events',
                  style: TextStyle(
                    fontSize: 24,
                    fontWeight: FontWeight.bold,
                    color: SportifyColors.primary,
                  ),
                ),
              ),
              Container(
                margin: const EdgeInsets.only(left: 8),
                padding: const EdgeInsets.all(8),
                decoration: const BoxDecoration(
                  shape: BoxShape.circle,
                  color: SportifyColors.primary,
                ),
                child: Text(
                  filteredEvents.length.toString(),
                  style: const TextStyle(
                    fontSize: 16,
                    fontWeight: FontWeight.bold,
                    color: Colors.white,
                  ),
                ),
              ),
            ],
          ),
          Expanded(
            child: ListView.builder(
              itemCount: filteredEvents.length,
              itemBuilder: (context, index) {
                final event = filteredEvents[index];

                return Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 4.0, vertical: 8.0),
                  child: Card(
                    child: ListTile(
                      title: Text(
                        '${event['name']+':'}',
                        style: const TextStyle(
                          fontSize: 18,
                          fontWeight: FontWeight.bold,
                          color: SportifyColors.text,
                        ),
                      ),
                      subtitle: Text(
                        '${event['date']}\n${event['address']}\n${event['description']}',
                        style: TextStyle(
                          fontSize: 16,
                          color: Colors.grey[600],
                        ),
                      ),
                    ),
                  ),
                );
              },
            ),
          ),
        ],
      ),
    );
  }
}