import 'package:flutter/material.dart';
import '../../components/dialogBox.dart';
import '../../components/textfield.dart';
import '../../components/main_button.dart';
import '../../models/auth/login_model.dart';
import '../../utils/sportify_colors.dart';
import '../../components/square_tile.dart';
import '../../models/authentication/strategies/email_password_auth.dart';
import '../../models/authentication/strategies/google_auth.dart';
import '../../controllers/auth/login_controller.dart';
import '../../controllers/app_controller.dart';
import 'package:connectivity_plus/connectivity_plus.dart';

class LoginPage extends StatefulWidget {
  final Function()? onTap;
  const LoginPage({super.key, required this.onTap});

  @override
  State<LoginPage> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  EmailPasswordAuth emailPasswordAuth = EmailPasswordAuth();
  GoogleAuth googleAuth = GoogleAuth();

  final emailController = TextEditingController();
  final passwordController = TextEditingController();

  final LoginController controller = LoginController(model: LoginModel());
  final appController = AppController();

  void logIn() async {
    showDialog(
      context: context,
      builder: (context) {
        return const Center(
          child: CircularProgressIndicator(),
        );
      },
    );
    try {
      // Check for internet connection
      var connectivityResult = await Connectivity().checkConnectivity();

      if (connectivityResult != ConnectivityResult.none) {
        // Internet connection
        await controller.login(emailController.text, passwordController.text);
        Navigator.pop(context);
      } else {
        // No internet connection
        Navigator.pop(context);
        showErrorMessage('Don\'t have internet connection', 'You need to have internet connection to log in');
      }
    } catch (e) {
      if (e.toString() == 'user-not-found') {
        Navigator.pop(context);
        showErrorMessage('Wrong email!', 'Check your email');
      } else if (e.toString() == 'wrong-password') {
        Navigator.pop(context);
        showErrorMessage('Wrong password!', 'Check your password');
      } else if (e.toString() == '[firebase_auth/invalid-email] The email address is badly formatted.') {
        Navigator.pop(context);
        showErrorMessage('Invalid email!', 'Expected format: example@mail.com');
      } else {
        Navigator.pop(context);
        showErrorMessage('Fill the blanks!', 'You need to fill every blank to log in');
      }
    }
  }

  void showErrorMessage(String title, String content) {
    showDialog(
        context: context,
        builder: (context) => DialogBox(
          title: title,
          content: content,
        )
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: SportifyColors.background,
        body: SafeArea(
            child: Center(
                child: SingleChildScrollView(
                  child: Column(
                      children: [
                        const SizedBox(height: 100),

                        //Logo
                        Image.asset(
                          'assets/images/logo.png',
                          width: 80,
                          height: 80,
                        ),
                        //Sportify
                        const Text(
                          'Sportify',
                          style: TextStyle(
                            fontFamily: 'Roboto',
                            color: SportifyColors.primary,
                            fontSize: 24,
                            fontWeight: FontWeight.bold,
                          ),
                        ),

                        const SizedBox(height: 50),

                        // Email TextField
                        MyTextField(
                            controller: emailController,
                            title: 'Email',
                            hintText: 'example@gmail.com',
                            obscureText: false
                        ),

                        const SizedBox(height: 10),

                        // Password TextField
                        MyTextField(
                            controller: passwordController,
                            title: 'Password',
                            hintText: '************',
                            obscureText: true
                        ),

                        //"Forgot password?"
                        Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 35.0, vertical: 15.0),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: [
                              Text(
                                'Forgot password?',
                                style: TextStyle(
                                  fontFamily: 'Roboto',
                                  color: Colors.grey[600],
                                ),
                              ),
                            ],
                          ),
                        ),

                        const SizedBox(height: 25),

                        //"Don't have an account? Sign up"
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text(
                              'Don\'t have an account? ',
                              style: TextStyle(color: Colors.grey[700]),
                            ),
                            const SizedBox(width: 4),
                            GestureDetector(
                              onTap: widget.onTap,
                              child: const Text(
                                'Sign up',
                                style: TextStyle(
                                  color: SportifyColors.primary,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                            ),
                          ],
                        ),

                        const SizedBox(height: 25),

                        //Login Button
                        MainButton(
                            text: 'Log in',
                            onTap: () {
                              appController.saveAuthStrategy("password");
                              controller.setAuthStrategy(emailPasswordAuth);
                              logIn();
                            }
                        ),

                        const SizedBox(height: 25),

                        // "or" divider
                        Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 25.0),
                          child: Row(
                            children: [
                              Expanded(
                                child: Divider(
                                  thickness: 1,
                                  color: Colors.grey[500],
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.symmetric(horizontal: 10.0),
                                child: Text(
                                  'or',
                                  style: TextStyle(color: Colors.grey[600]),
                                ),
                              ),
                              Expanded(
                                child: Divider(
                                  thickness: 1,
                                  color: Colors.grey[500],
                                ),
                              ),
                            ],
                          ),
                        ),

                        const SizedBox(height: 30),

                        //Google login button
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            // google button
                            SquareTile(
                                onTap: () {
                                  appController.saveAuthStrategy("google.com");
                                  controller.setAuthStrategy(googleAuth);
                                  logIn();
                                },
                                imagePath: 'assets/images/google.png'
                            ),
                          ],
                        ),
                      ]
                  ),
                )
            )
        )
    );
  }
}
