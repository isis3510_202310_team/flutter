import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter/material.dart';
import '../../components/dialogBox.dart';
import '../../components/textfield.dart';
import '../../components/main_button.dart';
import '../../utils/sportify_colors.dart';
import '../../models/authentication/strategies/email_password_auth.dart';
import '../../models/authentication/strategies/google_auth.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'dart:io';
import 'package:image_picker/image_picker.dart';
import '../../models/auth/register_model.dart';
import '../../controllers/auth/register_controller.dart';
import '../../models/sportify_user.dart';
import '../../controllers/app_controller.dart';
import 'package:intl/intl.dart';



class RegisterPage extends StatefulWidget {
  final Function()? onTap;
  const RegisterPage({super.key, required this.onTap});

  @override
  State<RegisterPage> createState() => _RegisterPageState();
}

class _RegisterPageState extends State<RegisterPage> {

  //AuthStrategies instances
  EmailPasswordAuth emailPasswordAuth = EmailPasswordAuth();
  GoogleAuth googleAuth = GoogleAuth();

  //ImageFile
  File? _imageFile;

  //Text editing controllers
  final fullNameController = TextEditingController();
  final birthDateController = TextEditingController();
  final bioController = TextEditingController();
  final phoneNumberController = TextEditingController();
  final emailController = TextEditingController();
  final passwordController = TextEditingController();

  final RegisterController controller = RegisterController(model: RegisterModel());
  final appController = AppController();

  @override
  void initState() {
    super.initState();
    loadDataFromCache();
  }

  Future<void> loadDataFromCache() async {
    String? formInfo = await appController.getFormInfoFromCache();
    if (formInfo != null) {
      print(formInfo);
      setControllerValues(formInfo);
    }
  }

  void setControllerValues(String formInfo) {
    List<String> parts = formInfo.split(',');
    for (String part in parts) {
      List<String> keyValue = part.split(':');
      String key = keyValue[0].trim();
      String value = keyValue[1].trim();

      if (key == '{fullName') {
        fullNameController.text = value;
      } else if (key == 'birthDate') {
        birthDateController.text = value;
      } else if (key == 'bio') {
        bioController.text = value;
      } else if (key == 'phone') {
        phoneNumberController.text = value;
      } else if (key == 'email') {
        emailController.text = value;
      } else if (key == 'password') {
        passwordController.text = value;
      }
    }
  }

  //Signup method
  void signUp() async {
    //Loading
    showDialog(
      context: context,
      builder: (context) {
        return const Center(
          child: CircularProgressIndicator(),
        );
      },
    );

    //Create user
    try {
      //Input validations
      final phoneNumber = int.tryParse(phoneNumberController.text);
      RegExp birthDateRegExp = RegExp(r'^(?:(?:31(\/|-|\.)(?:0?[13578]|1[02]))\1|(?:(?:29|30)(\/|-|\.)(?:0?[13-9]|1[0-2])\2))(?:(?:1[6-9]|[2-9]\d)?\d{2})$|^(?:29(\/|-|\.)0?2\3(?:(?:(?:1[6-9]|[2-9]\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))$|^(?:0?[1-9]|1\d|2[0-8])(\/|-|\.)(?:(?:0?[1-9])|(?:1[0-2]))\4(?:(?:1[6-9]|[2-9]\d)?\d{2})$');

      if (fullNameController.text.isEmpty || birthDateController.text.isEmpty || bioController.text.isEmpty) {
        Navigator.pop(context);
        showErrorMessage('Fill every blank!', 'You need to fill every blank to sign up');
        return;
      }

      else if (!birthDateRegExp.hasMatch(birthDateController.text)) {
        Navigator.pop(context);
        showErrorMessage('Birth date is not valid', 'Excepted format: DD/MM/YYYY');
        return;
      }

      else if(phoneNumber == null){
        Navigator.pop(context);
        showErrorMessage('Phone number is not valid', 'You need to write a valid number');
        return;
      }

      else{

        var connectivityResult = await Connectivity().checkConnectivity();

        if (connectivityResult != ConnectivityResult.none) {
          // Internet connection
          execSignUp();
          Navigator.pop(context);
        } else {
          Navigator.pop(context);
          showErrorMessage('No internet connection!', 'You need to have internet connection to sign up');
          // No internet connection
          final formUser = SportifyUser();
          final phoneNumber = int.tryParse(phoneNumberController.text);
          if (phoneNumber != null) {
            formUser.fullName = fullNameController.text;
            formUser.birthDate = birthDateController.text;
            formUser.bio = bioController.text;
            formUser.phone = phoneNumber;
            formUser.email = emailController.text;
            formUser.password = passwordController.text;
            formUser.avatarUrl = '';
          } else {
            showErrorMessage('Phone number is not valid', 'You need to write a valid number');
          }
          saveFormInfoInCache(formUser.toString());
        }
      }
    } catch (e) {
      if (e.toString() == 'email-already-in-use') {
        Navigator.pop(context);
        showErrorMessage('Email already in use!', 'Write another email');
      }

      else if (e.toString() == 'invalid-email') {
        Navigator.pop(context);
        showErrorMessage('Invalid email!', 'Expected format: example@mail.com');
      }

      else if (e.toString() == 'weak-password') {
        Navigator.pop(context);
        showErrorMessage('Invalid password!', 'The password must be at least 6 characters long');
      }

      else if (e.toString() == 'unknown') {
        Navigator.pop(context);
        showErrorMessage('Fill the blanks!', 'You need to fill every blank to sign up');
      }
    }
  }

  void saveFormInfoInCache (String formInfo) {
    appController.saveFormInfoInCache(formInfo);
  }

  void execSignUp() async {
    await controller.signup(
        emailController.text,
        passwordController.text
    );

    //Store in DB
    if (_imageFile != null) {
      try {
        final currentUser = FirebaseAuth.instance.currentUser;
        final userId = currentUser?.uid;

        final user = SportifyUser();

        final imageUrl = await controller.uploadAvatar(_imageFile!);

        final phoneNumber = int.tryParse(phoneNumberController.text);
        if (phoneNumber != null) {
          user.fullName = fullNameController.text;
          user.birthDate = birthDateController.text;
          user.bio = bioController.text;
          user.phone = phoneNumber;
          user.email = currentUser?.email ?? '';
          user.password = '';
          user.avatarUrl = imageUrl;
          controller.save(user);
          appController.deleteFormInfoInCache();
        } else {
          showErrorMessage('Phone number is not valid', 'You need to write a valid number');
        }
      } catch (e) {
        showErrorMessage('Error uploading image!', 'Error uploading image!');
      }
    } else {
      // No image to upload
      final currentUser = FirebaseAuth.instance.currentUser;
      final userId = currentUser?.uid;

      final user = SportifyUser();

      final phoneNumber = int.tryParse(phoneNumberController.text);
      if (phoneNumber != null) {
        user.fullName = fullNameController.text;
        user.birthDate = birthDateController.text;
        user.bio = bioController.text;
        user.phone = phoneNumber;
        user.email = currentUser?.email ?? '';
        user.password = '';
        user.avatarUrl = '';
        controller.save(user);
        appController.deleteFormInfoInCache();
      } else {
        showErrorMessage('Phone number is not valid', 'You need to write a valid number');
      }
    }
  }

  void showErrorMessage(String title, String content) {
    showDialog(
        context: context,
        builder: (context) => DialogBox(
          title: title,
          content: content,
        )
    );
  }

  void _selectImage() async {
    final ImagePicker _picker = ImagePicker();
    final XFile? pickedFile = await _picker.pickImage(
      source: ImageSource.gallery,
      maxWidth: 800,
      maxHeight: 800,
      imageQuality: 80,
    );
    if (pickedFile != null) {
      setState(() {
        _imageFile = File(pickedFile.path);
      });
    }
  }

  void _takePicture() async {
    final ImagePicker _picker = ImagePicker();
    final XFile? pickedFile = await _picker.pickImage(
      source: ImageSource.camera,
      maxWidth: 800,
      maxHeight: 800,
      imageQuality: 80,
    );
    if (pickedFile != null) {
      setState(() {
        _imageFile = File(pickedFile.path);
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: SportifyColors.background,
        body: SafeArea(
            child: Center(
                child: SingleChildScrollView(
                  child: Column(
                      children: [
                        //Photo
                        InkWell(
                          onTap: () {},
                          child: Stack(
                            alignment: AlignmentDirectional.bottomEnd,
                            children: [
                              CircleAvatar(
                                radius: 70,
                                backgroundColor: SportifyColors.tertiary,
                                backgroundImage: _imageFile != null ? FileImage(_imageFile!) : null,
                                child: _imageFile == null
                                    ? const Icon(Icons.camera_alt, size: 40, color: SportifyColors.primary)
                                    : null,
                              ),
                              Container(
                                width: 30,
                                height: 30,
                                decoration: const ShapeDecoration(
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.all(Radius.circular(8)),
                                  ),
                                  color: SportifyColors.primary,
                                ),
                                child: IconButton(
                                    onPressed: () {
                                      showModalBottomSheet(
                                        context: context,
                                        builder: (context) {
                                          return Container(
                                            height: 120,
                                            child: Column(
                                              children: <Widget>[
                                                ListTile(
                                                  leading: const Icon(Icons.photo_library),
                                                  title: const Text('Photo Library'),
                                                  onTap: () {
                                                    Navigator.pop(context);
                                                    _selectImage();
                                                  },
                                                ),
                                                ListTile(
                                                  leading: const Icon(Icons.photo_camera),
                                                  title: const Text('Camera'),
                                                  onTap: () {
                                                    Navigator.pop(context);
                                                    _takePicture();
                                                  },
                                                ),
                                              ],
                                            ),
                                          );
                                        },
                                      );
                                    },
                                    iconSize: 18,
                                    padding: const EdgeInsets.all(1),
                                    icon: const Icon(Icons.add, color: Colors.white)
                                ),
                              ),
                            ],
                          ),
                        ),

                        const SizedBox(height: 30),

                        // BirthDate TextField
                        MyTextField(
                            controller: fullNameController,
                            title: 'Full name',
                            hintText: 'Enter your full name',
                            obscureText: false
                        ),

                        // BirthDate TextField
                        Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 35.0, vertical: 5.0),
                            child: Row(
                                children: [
                                  Expanded(
                                      child: TextField(
                                          controller: birthDateController,
                                            decoration: InputDecoration(
                                              floatingLabelBehavior: FloatingLabelBehavior.always,
                                              labelText: "Birth date",
                                              labelStyle: const TextStyle(
                                                fontFamily: 'Roboto',
                                                color: SportifyColors.primary,
                                                fontSize: 20,
                                                fontWeight: FontWeight.bold,
                                              ),
                                              hintText: "Pick your birth date",
                                              hintStyle: TextStyle(
                                                fontFamily: 'Roboto',
                                                color: Colors.grey[500],
                                              ),
                                              enabledBorder: const OutlineInputBorder(
                                                borderSide: BorderSide(
                                                  color: SportifyColors.primary,
                                                  width: 2, // increase the width to make the border thicker
                                                ),
                                              ),
                                              focusedBorder: const OutlineInputBorder(
                                                borderSide: BorderSide(
                                                  color: SportifyColors.primary,
                                                  width: 2, // increase the width to make the border thicker
                                                ),
                                              ),
                                              fillColor: SportifyColors.background,
                                              filled: true,
                                            ),
                                          readOnly: true,
                                          onTap: () async {
                                            DateTime? pickedDate = await showDatePicker(
                                              context: context,
                                              initialDate: DateTime.now(),
                                              firstDate: DateTime(1900),
                                              lastDate: DateTime.now(),
                                              builder: (BuildContext context, Widget? child) {
                                                return Theme(
                                                  data: ThemeData.light().copyWith(
                                                    colorScheme: ColorScheme.light(
                                                      primary: SportifyColors.primary,
                                                      onPrimary: Colors.white,
                                                      surface: SportifyColors.background,
                                                      onSurface: Colors.black,
                                                    ),
                                                    textTheme: ThemeData.light().textTheme.copyWith(
                                                      titleLarge: const TextStyle(
                                                        fontFamily: 'Roboto',
                                                        color: Colors.black,
                                                        fontSize: 20,
                                                        fontWeight: FontWeight.bold,
                                                      ),
                                                      bodyMedium: const TextStyle(
                                                        fontFamily: 'Roboto',
                                                        color: Colors.black,
                                                        fontSize: 16,
                                                      ),
                                                    ),
                                                    dialogBackgroundColor: Colors.white,
                                                  ),
                                                  child: child!,
                                                );
                                              },
                                            );

                                            if(pickedDate != null ){
                                              String formattedDate = DateFormat('dd/MM/yyyy').format(pickedDate);

                                              setState(() {
                                                birthDateController.text = formattedDate;
                                              });
                                            }
                                          }
                                        ),
                                    ),
                                ],
                            ),
                        ),

                        // Bio TextField
                        MyTextField(
                            controller: bioController,
                            title: 'Bio',
                            hintText: 'Enter information for your bio',
                            obscureText: false
                        ),

                        const SizedBox(height: 10),

                        // PhoneNumber TextField
                        MyTextField(
                            controller: phoneNumberController,
                            title: 'Phone number',
                            hintText: 'Enter your phone number',
                            obscureText: false
                        ),

                        const SizedBox(height: 10),

                        // Email TextField
                        MyTextField(
                            controller: emailController,
                            title: 'Email',
                            hintText: 'Enter a valid email',
                            obscureText: false
                        ),

                        const SizedBox(height: 10),

                        // Password TextField
                        MyTextField(
                            controller: passwordController,
                            title: 'Password',
                            hintText: 'Enter a password',
                            obscureText: true
                        ),

                        const SizedBox(height: 10),

                        //"Don't have an account? Sign up"
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text(
                              'Already have an account? ',
                              style: TextStyle(color: Colors.grey[700]),
                            ),
                            const SizedBox(width: 4),
                            GestureDetector(
                              onTap: widget.onTap,
                              child: const Text(
                                'Log in',
                                style: TextStyle(
                                  color: SportifyColors.primary,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                            ),
                          ],
                        ),

                        const SizedBox(height: 25),

                        //Sign up Button
                        MainButton(
                            text: 'Sign up',
                            onTap: () {
                              appController.saveAuthStrategy("password");
                              controller.setAuthStrategy(emailPasswordAuth);
                              signUp();
                            }
                        ),
                      ]
                  ),
                )
            )
        )
    );
  }
}

