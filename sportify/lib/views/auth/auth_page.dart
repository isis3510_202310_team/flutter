import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'login_or_register_page.dart';
import '../home_page.dart';
import '../../models/authentication/auth_manager.dart';
import '../../models/authentication/strategies/email_password_auth.dart';
import '../../models/authentication/strategies/google_auth.dart';

class AuthPage extends StatelessWidget {
  const AuthPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: StreamBuilder<User?>(
        stream: FirebaseAuth.instance.authStateChanges(),
        builder: (context, snapshot) {
          //User logged in
          if (snapshot.hasData) {
            EmailPasswordAuth emailPasswordAuth = EmailPasswordAuth();
            GoogleAuth googleAuth = GoogleAuth();

            AuthManager authManager = AuthManager(emailPasswordAuth);

            final user = FirebaseAuth.instance.currentUser;
            if (user != null) {
              final providerId = user.providerData.first.providerId;
              if(providerId == 'password'){
                authManager = AuthManager(emailPasswordAuth);
              }
              else if(providerId == 'google.com') {
                authManager = AuthManager(googleAuth);
              }
            }
            // HomePage is temporary.
            // After logging in events view should be displayed.
            return const HomePage();
          }

          //User NOT logged in
          else {
            return const LoginOrRegisterPage();
          }

        },
      ),
    );
  }
}