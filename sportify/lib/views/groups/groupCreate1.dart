import 'dart:io';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:image_gallery_saver/image_gallery_saver.dart';
import 'package:image_picker/image_picker.dart';
import 'package:sqflite/sqflite.dart';

import '../../components/textFormField.dart';
import '../../controllers/app_controller.dart';
import '../../controllers/createEvent/eventController.dart';
import '../../controllers/createEvent/sportsController.dart';
import '../../controllers/groups/group_controller.dart';
import '../../controllers/notifications/notification_controller.dart';
import '../../models/createEvent/sports.dart';
import '../../models/groups/group.dart';
import '../../utils/sportify_colors.dart';

class CreateGroup1 extends StatefulWidget {
  @override
  _CreateGroup1State createState() => _CreateGroup1State();
}

class _CreateGroup1State extends State<CreateGroup1> {
  String groupName = '';
  String groupDescription = '';
  File? _imageFile;
  String? imagePath;
  final _formKey = GlobalKey<FormState>();
  AppController appController = AppController();
  NotificationController _notificationController = NotificationController();
  GroupController groupController = GroupController();
  SportsController sportsController = SportsController(ListSports());
  final _sports = ListSports().getSports();
  final Map<String, bool> _selectedSports = {};

  @override
  void initState() {
    super.initState();
    _sports.forEach((sport) {
      _selectedSports[sport.name] = false;
    });
  }

  void _saveImage() async {
    final String fileName = groupName! + '.jpg';
    final String filePath = _imageFile!.path;
    final result = await ImageGallerySaver.saveFile(filePath, name: fileName);
    if (result['isSuccess']) {
      imagePath = filePath;
    }
  }

  void _selectImage() async {
    final ImagePicker _picker = ImagePicker();
    final XFile? pickedFile = await _picker.pickImage(
      source: ImageSource.gallery,
      maxWidth: 800,
      maxHeight: 800,
      imageQuality: 80,
    );
    if (pickedFile != null) {
      setState(() {
        _imageFile = File(pickedFile.path);
        imagePath = pickedFile.path;
      });
    }
  }

  void _takePicture() async {
    final ImagePicker _picker = ImagePicker();
    final XFile? pickedFile = await _picker.pickImage(
      source: ImageSource.camera,
      maxWidth: 800,
      maxHeight: 800,
      imageQuality: 80,
    );
    if (pickedFile != null) {
      setState(() {
        _imageFile = File(pickedFile.path);
        _saveImage();
      });
    }
  }


  void OnCreateGroup() async {
    var sports = _selectedSports.keys.where((
        sport) => _selectedSports[sport]!).toList();
    final user = FirebaseAuth.instance.currentUser;
    final connectivityResult = await appController.getConnectionStatus();
    if (connectivityResult == ConnectivityResult.none) {
      Database database = await openDatabase(
      'temporal_groups.db',
      version: 1,
      onCreate: (Database db, int version) async {
        await db.execute('CREATE TABLE temporal_groups ('
            'id INTEGER PRIMARY KEY,'
            'owner TEXT,'
            'name TEXT,'
            'description TEXT,'
            'image TEXT,'
            'sports TEXT,'
            'members TEXT'
            ')');
      },
    );
      final group = Group(
        name: groupName,
        description: groupDescription,
        image: imagePath,
        sports: sports,
        members: [user!.uid],
        owner: user.uid,
      );
      int id = await database.insert(
        'temporal_groups',
        group.toDB(),
        conflictAlgorithm: ConflictAlgorithm.replace,
      );
      groupController.uploadGroup(id, database);
      _notificationController.showNotification("Connection error", "Waiting for connection");
      Navigator.pushNamedAndRemoveUntil(context, '/home', (route) => false);

    } else {
      final imageUrl = await groupController.uploadImage(_imageFile!);
      final group = Group(
        name: groupName,
        description: groupDescription,
        image: imageUrl,
        sports: sports,
        members: [user!.uid],
        owner: user.uid,
      );
      final databaseReference = FirebaseDatabase.instance.ref();
      databaseReference
          .child('groups')
          .push()
          .set(group.toJson());
      Navigator.pop(context);
      _notificationController.showNotification(
          "Success", "Group created successfully");
    }
  }

  Widget _buildSportButton(Map<String, String> sport) {
    final isSelected = _selectedSports[sport['name']!]!;
    final color = isSelected ? SportifyColors.primary : Colors.grey;

    return ElevatedButton(
      style: ButtonStyle(
        elevation: MaterialStateProperty.all<double>(6),
        backgroundColor: MaterialStateProperty.all<Color>(color),
        shape: MaterialStateProperty.all<RoundedRectangleBorder>(
          RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(20),
          ),
        ),
      ),
      onPressed: () {
        setState(() {
          _selectedSports[sport['name']!] = !isSelected;
        });
      },
      child: Row(
        mainAxisSize: MainAxisSize.min,
        children: [
          Text(
            sport['emoji']!,
            style: TextStyle(fontSize: 20),
          ),
          SizedBox(width: 8),
          Text(
            sport['name']!,
            style: TextStyle(
              fontFamily: 'Roboto',
              color: Colors.white,
              fontSize: 20,
            ),
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: SportifyColors.background,
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: SportifyColors.background,
          iconTheme: IconThemeData(
            color: SportifyColors.primary,
          ),
        ),
        body: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Padding(
                padding: const EdgeInsets.only(top: 16.0, bottom: 16.0),
                child: Text(
                  'Add an image for your group',
                  style: TextStyle(
                    fontFamily: 'Roboto',
                    color: SportifyColors.text,
                    fontSize: 25,
                    fontWeight: FontWeight.bold,
                  ),
                  textAlign: TextAlign.center,
                ),
              ),
              InkWell(
                onTap: () {},
                child: Stack(
                  alignment: AlignmentDirectional.bottomEnd,
                  children: [
                    //avatar rectangular
                    Container(
                      width: 200,
                      height: 150,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(8),
                        color: SportifyColors.primary,
                      ),
                      child: _imageFile != null
                          ? ClipRRect(
                        borderRadius: BorderRadius.circular(8),
                        child: Image.file(
                          _imageFile!,
                          fit: BoxFit.cover,
                        ),
                      )
                          : const Icon(
                        Icons.add_a_photo,
                        color: Colors.white,
                        size: 50,
                      ),
                    ),
                    Container(
                      margin: const EdgeInsets.only(right: 8, bottom: 8),
                      width: 25,
                      height: 25,
                      decoration: const ShapeDecoration(
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.all(Radius.circular(8)),
                        ),
                        color: SportifyColors.primary,
                      ),
                      child: IconButton(
                          onPressed: () {
                            showModalBottomSheet(
                              context: context,
                              builder: (context) {
                                return Container(
                                  height: 120,
                                  child: Column(
                                    children: <Widget>[
                                      ListTile(
                                        leading:
                                        const Icon(Icons.photo_library),
                                        title: const Text('Photo Library'),
                                        onTap: () {
                                          Navigator.pop(context);
                                          _selectImage();
                                        },
                                      ),
                                      ListTile(
                                        leading:
                                        const Icon(Icons.photo_camera),
                                        title: const Text('Camera'),
                                        onTap: () {
                                          Navigator.pop(context);
                                          _takePicture();
                                        },
                                      ),
                                    ],
                                  ),
                                );
                              },
                            );
                          },
                          iconSize: 18,
                          padding: const EdgeInsets.all(1),
                          icon: const Icon(Icons.add, color: Colors.white)),
                    ),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 16.0),
                child: Text(
                  'What is the name of your group?',
                  style: TextStyle(
                    fontFamily: 'Roboto',
                    color: SportifyColors.text,
                    fontSize: 25,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
              Form(
                key: _formKey,
                child: Padding(
                  padding:
                  const EdgeInsets.only(top: 16.0, left: 16.0, right: 16.0),
                  child: SportifyTextFormFieldDecorator(
                    context: context,
                    hintText: 'Group name',
                    validationText: 'Please enter a name for your group',
                    onChanged: (value) {
                      setState(() {
                        groupName = value;
                      });
                    },
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 16.0, bottom: 16.0),
                child: Text(
                  'What is your group about?',
                  style: TextStyle(
                    fontFamily: 'Roboto',
                    color: SportifyColors.text,
                    fontSize: 25,
                    fontWeight: FontWeight.bold,
                  ),
                  textAlign: TextAlign.center,
                ),
              ),
              Container(
                width: double.infinity,
                height: 150,
                padding: EdgeInsets.symmetric(horizontal: 16),
                child: SportifyTextFormFieldDecorator(
                  context: context,
                  hintText: 'Group description',
                  onChanged: (value) {
                    setState(() {
                      groupDescription = value;
                    });
                  },
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 16.0, bottom: 16.0),
                child: Text(
                  'Which sports do you want to include?',
                  style: TextStyle(
                    fontFamily: 'Roboto',
                    color: SportifyColors.text,
                    fontSize: 25,
                    fontWeight: FontWeight.bold,
                  ),
                  textAlign: TextAlign.center,
                ),
              ),
              Wrap(
                runSpacing: 5,
                spacing: 5,
                alignment: WrapAlignment.center,
                children:
                sportsController.getSportsMap().map((sport) => _buildSportButton(sport)).toList(),
              ),
            ],
          ),
        ),
        bottomNavigationBar: BottomAppBar(
          elevation: 0,
          color: Colors.transparent,
          height: 100,
          child: Center(
            child: Container(
              constraints: BoxConstraints(maxWidth: 200),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(35),
                color: SportifyColors.primary,
              ),
              child: TextButton(
                onPressed: () {
                  if (imagePath == null) {
                    ScaffoldMessenger.of(context).showSnackBar(
                      SnackBar(
                        content: Text(
                          'Please select an image',
                          style: TextStyle(
                            fontFamily: 'Roboto',
                            color: Colors.white,
                            fontSize: 20,
                          ),
                        ),
                        backgroundColor: SportifyColors.primary,
                      ),
                    );
                  } else{
                  if (_formKey.currentState!.validate()) {
                    if (_selectedSports.values.contains(true)) {
                      OnCreateGroup();
                    }else {
                      ScaffoldMessenger.of(context).showSnackBar(
                        SnackBar(
                          content: Text(
                            'Please select at least one sport',
                            style: TextStyle(
                              fontFamily: 'Roboto',
                              color: Colors.white,
                              fontSize: 20,
                            ),
                          ),
                          backgroundColor: SportifyColors.primary,
                        ),
                      );
                    }
                  }
                  }
                },
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    SizedBox(width: 8),
                    Text(
                      'Create Group',
                      style: TextStyle(
                        fontFamily: 'Roboto',
                        fontSize: 20,
                        fontWeight: FontWeight.bold,
                        color: SportifyColors.background,
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
