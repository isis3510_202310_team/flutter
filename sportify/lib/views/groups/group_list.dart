import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';
import 'package:sportify/views/groups/groupCreate1.dart';
import '../../components/dialogBox.dart';
import '../../controllers/app_controller.dart';
import '../../utils/sportify_colors.dart';
import 'package:cached_network_image/cached_network_image.dart';


class GroupList extends StatefulWidget {
  const GroupList({Key? key}) : super(key: key);

  @override
  State<GroupList> createState() => _GroupListState();
}

class _GroupListState extends State<GroupList> {
  List<dynamic> myGroups = [];
  AppController appController = AppController();
  bool _isMounted = false;

  @override
  void initState() {
    super.initState();
    _isMounted = true;
    getGroups(context);
  }

  @override
  void dispose() {
    _isMounted = false;
    super.dispose();
  }

  void getGroups(BuildContext context) async {
    final user = FirebaseAuth.instance.currentUser;
    final uid = user!.uid;
    final ref = FirebaseDatabase.instance.ref().child("groups");
    ref.onValue.listen((group) {
      final Object? values = group.snapshot.value;
      if (_isMounted && values != null) {
        final Map<dynamic, dynamic> map = values as Map<dynamic, dynamic>;
        final List<dynamic> groups = [];
        map.forEach((key, value) {
          groups.add(value);
        });
        setState(() {
          myGroups = groups;
        });
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: SportifyColors.background,
        iconTheme: IconThemeData(
          color: SportifyColors.primary,
        ),
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: EdgeInsets.fromLTRB(12, 10, 14, 0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  'Groups:',
                  style: TextStyle(
                    fontFamily: 'Roboto',
                    color: SportifyColors.text,
                    fontSize: 30,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                Transform.scale(
                  scale: 0.8, // Adjust the scale value as needed
                  child: Material(
                    elevation: 1,
                    shape: CircleBorder(),
                    clipBehavior: Clip.hardEdge,
                    color: SportifyColors.primary,
                    child: IconButton(
                      onPressed: () async {
                        final connectivityResult = await appController.getConnectionStatus();
                        if (connectivityResult == ConnectivityResult.none) {
                          showDialog(
                            context: context,
                            builder: (context) => DialogBox(
                              title: 'Don\'t have internet connection',
                              content: 'You need to have internet connection to create an event',
                            ),
                          );
                        } else {
                        Navigator.of(context).push(
                          MaterialPageRoute(
                              builder: (_) => CreateGroup1()),
                        );
                        }
                      },
                      icon: Icon(
                        Icons.add,
                        color: Colors.white,
                        size: 25,
                      ),
                    ),
                  ),
                )
              ],
            ),
          ),
          Expanded(
            child: ListView.builder(
              padding: EdgeInsets.zero,
              itemCount: myGroups.length,
              itemBuilder: (context, index) {
                final group = myGroups[index];
                return Container(
                  height: 120,
                  margin: EdgeInsets.all(10),
                  decoration: BoxDecoration(
                    color: SportifyColors.quinary,
                    borderRadius: BorderRadius.circular(20),
                    boxShadow: [
                      BoxShadow(
                        color: Colors.grey.withOpacity(0.3),
                        spreadRadius: 1,
                        blurRadius: 4,
                        offset: Offset(0, 3),
                      ),
                    ],
                  ),
                  child: Stack(
                    children: [
                      Positioned.fill(
                        left: 0,
                        top: 0,
                        bottom: 0,
                        child: FractionallySizedBox(
                          alignment: Alignment.centerLeft,
                          widthFactor: 1 / 3,
                          child: ClipRRect(
                            child: ClipRRect(
                              borderRadius: BorderRadius.only(
                                topLeft: Radius.circular(20),
                                bottomLeft: Radius.circular(20),
                              ),
                              child: CachedNetworkImage(
                                imageUrl: group['image'],
                                fit: BoxFit.cover,
                                placeholder: (context, url) =>
                                    CircularProgressIndicator(),
                                errorWidget: (context, url, error) =>
                                    Icon(Icons.error),
                              ),
                            ),
                          ),
                        ),
                      ),
                      Align(
                        alignment: Alignment.center,
                        child: Container(
                          margin: EdgeInsets.only(left: MediaQuery.of(context).size.width / 3),
                          child: ListTile(
                            title: Text(
                              group['name'],
                              style: TextStyle(
                                fontFamily: 'Roboto',
                                color: SportifyColors.text,
                                fontSize: 16,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                            subtitle: Text(
                              group['description'],
                              maxLines: 4,
                              overflow: TextOverflow.ellipsis,
                              style: TextStyle(
                                fontFamily: 'Roboto',
                                fontSize: 14,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                );
              },
            ),
          ),
        ],
      ),
    );
  }
}