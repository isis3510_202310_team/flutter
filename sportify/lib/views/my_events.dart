import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart';
import 'package:intl/intl.dart';

import '../utils/sportify_colors.dart';

class MyEventsPage extends StatefulWidget {
  const MyEventsPage({Key? key}) : super(key: key);

  @override
  State<MyEventsPage> createState() => _MyEventsPageState();
}

class _MyEventsPageState extends State<MyEventsPage> {
  List<dynamic> myEvents = [];
  List<dynamic> myFilteredEvents = [];

  @override
  void initState() {
    super.initState();
    getEvents(context);
  }

  void _selectFilter() {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return SimpleDialog(
          title: const Text('Filter events'),
          children: [
            ListTile(
              leading: Icon(Icons.calendar_today),
              title: const Text('Dia'),
              onTap: () {
                final tomorrow = DateTime.now().add(Duration(days: 1));
                print("HERE");
                final filteredEvents = myEvents.where((event) {

                  final eventDate = event["date"];
                  return eventDate.day == tomorrow.day &&
                      eventDate.month == tomorrow.month &&
                      eventDate.year == tomorrow.year;
                }).toList();
                setState(() {
                  myFilteredEvents = filteredEvents;
                });
                Navigator.pop(context);
              },
            ),
            ListTile(
              leading: Icon(Icons.calendar_view_week),
              title: const Text('Semana'),
              onTap: () {
                final nextWeek = DateTime.now().add(Duration(days: 7));
                final filteredEvents = myEvents.where((event) {
                  final eventDate = event["date"];
                  final eventWeek = eventDate.add(Duration(
                      days: 7 - eventDate.weekday as int)); // Lunes de la semana del evento
                  return eventWeek.isBefore(nextWeek) &&
                      eventWeek.isAfter(DateTime.now());
                }).toList();
                setState(() {
                  myFilteredEvents = filteredEvents;
                });
                Navigator.pop(context);
              },
            ),
            ListTile(
              leading: Icon(Icons.calendar_view_month),
              title: const Text('Mes'),
              onTap: () {
                final nextMonth = DateTime.now().add(Duration(days: 30));
                final filteredEvents = myEvents.where((event) {
                  final eventDate = event["date"];
                  return eventDate.isBefore(nextMonth) &&
                      eventDate.isAfter(DateTime.now());
                }).toList();
                setState(() {
                  myFilteredEvents = filteredEvents;
                });
                Navigator.pop(context);
              },
            ),
          ],
        );
      },
    );
  }



  void getEvents(BuildContext context) async {
    final user = FirebaseAuth.instance.currentUser;
    final uid = user!.uid;
    final ref = FirebaseDatabase.instance.ref().child("events");
    ref.onValue.listen((event) {
      final Object? values = event.snapshot.value;
      if (values != null) {
        final Map<dynamic, dynamic> map = values as Map<dynamic, dynamic>;
        final List<dynamic> events = [];
        map.forEach((key, value) {
          if (value["idUser"] == uid) {
            final hourFormat = DateFormat('h:mm a');
            final startTime = hourFormat.parse(value["startTime"]);
            final startHour = startTime.hour;

            final separar = value["date"].split("/");
            if (separar[0].length == 1) {
              separar[0] = "0" + separar[0];
            }
            if (separar[1].length == 1) {
              separar[1] = "0" + separar[1];
            }
            final parsedDate =
            DateTime.parse("${separar[2]}-${separar[1]}-${separar[0]}");

            final startDate = DateTime(parsedDate.year, parsedDate.month,
                parsedDate.day, startHour, startTime.minute);

            value['date'] = startDate;

            events.add(value);
          }
        });
        setState(() {
          myEvents = events;
          myFilteredEvents = events;
        });
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('My Events'),
        backgroundColor: SportifyColors.primary,
        actions: [
          IconButton(onPressed: _selectFilter, icon: const Icon(Icons.filter_alt_outlined))
        ],
      ),
      body: ListView.builder(
        itemCount: myFilteredEvents.length,
        itemBuilder: (context, index) {
          final event = myFilteredEvents[index];

          return Card(
            child: ListTile(
              title: Text(event['name']),
              subtitle: Text('${event['date']}\n${event['address']}'),
            ),
          );
        },
      ),
    );
  }
}
