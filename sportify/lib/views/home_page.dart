import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:firebase_database/ui/firebase_animated_list.dart';
import '../components/navigation_bar.dart';
import '../components/clock.dart';
import '../controllers/app_controller.dart';
import '../models/authentication/strategies/email_password_auth.dart';
import '../models/authentication/strategies/google_auth.dart';
import '../utils/sportify_colors.dart';
import '../models/authentication/auth_manager.dart';

class HomePage extends StatefulWidget{

  const HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();

}

class _HomePageState extends State<HomePage> {
  late String? authStrategy;

  TextEditingController searchController = TextEditingController();
  String search = "";
  final ref = FirebaseDatabase.instance.ref().child("events");
  final auth = FirebaseAuth.instance;

  late EmailPasswordAuth emailPasswordAuth = EmailPasswordAuth();
  late GoogleAuth googleAuth = GoogleAuth();

  late AuthManager authManager;

  final appController = AppController();

  @override
  void initState() {
    super.initState();

    appController.getAuthStrategy().then((value) => setState(() {
      authStrategy = value;
      if (authStrategy == 'password') {
        authManager = AuthManager(emailPasswordAuth);
      } else if (authStrategy == 'google.com') {
        authManager = AuthManager(googleAuth);
      }
    }));
  }

  void logOut() {
    authManager.logout();
    Navigator.popAndPushNamed(context, '/auth');
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          title: const Text('Event List'),
          backgroundColor: SportifyColors.primary,
          actions: [

          ],
      ),
      bottomNavigationBar: NavigationBarBottom(),
      body: Column(children: [
        Expanded(child: Column(
          children:[
            TextFormField(
              controller: searchController,
              decoration: InputDecoration(
                hintStyle: const TextStyle(color: Colors.grey),
                hintText: "Search an event by name",
                prefixIcon: const Icon(Icons.search, color: SportifyColors.text),
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(10.0)
                ),
              ),
              onChanged: (String val){
                search = val;
              },
            ),
            Expanded(child: _eventList()),
          ]
        ))
      ])
    );
  }

  Widget _eventList(){
    return Center(
      child: FirebaseAnimatedList(
        query: ref,
        itemBuilder: (BuildContext context, DataSnapshot snapshot, Animation<double> animation, int index){
          var value = Map<String,dynamic>.from(snapshot.value as Map);
          var address = value["address"];
          var date = value["date"];
          var desc = value["description"];
          var name = value["name"];
          var startTime = value["startTime"];
          var endTime = value["endTime"];
          return _event(address, date, desc, startTime, endTime, name);
        },
      ),
    );
  }

  Widget _event(pAddress,pDate,pDesc,pTimeStart,pTimeEnd,pName){
    return Center(
      child: Container(
        margin: const EdgeInsets.fromLTRB(10,10,10,0),
        padding: const EdgeInsets.fromLTRB(10,10,10,0),
        height: 200,
        decoration: BoxDecoration(
          border: Border.all(width: 1, color: Colors.grey),
          borderRadius: BorderRadius.circular(12),
        ),
        child: Column(
          children: [
            Row(
              children: [
                Container(
                  padding: const EdgeInsets.fromLTRB(10,10,10,10),
                  alignment: Alignment.topLeft,
                  child: Text(
                    pName+':',
                    style: const TextStyle(
                      color: SportifyColors.text,
                      fontWeight: FontWeight.bold,
                      fontSize: 20,
                    ),
                  ),
                ),
                Container(
                  padding: const EdgeInsets.fromLTRB(10,10,10,10),
                  alignment: Alignment.topRight,
                  child: TextButton(
                      style: ButtonStyle(
                          alignment: Alignment.center,
                          padding: MaterialStateProperty.all<EdgeInsets>(EdgeInsets.fromLTRB(15,10,15,10)),
                          backgroundColor: MaterialStateProperty.all<Color>(SportifyColors.secondary),
                          shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                            RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(18.0),
                                side: BorderSide(color: SportifyColors.secondary)
                            ),
                          )
                      ),
                      onPressed: (){},
                      child: const Text(
                        'Details',
                        style: TextStyle(
                          color: SportifyColors.primary,
                        ),
                      )
                  )
                ),
              ],
            ),
            Container(
              padding: EdgeInsets.all(15.0),
              height: 120,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(12),
                image: DecorationImage(
                  image: AssetImage('assets/images/image-placeholder.jpg'),
                  fit: BoxFit.fitWidth,
                ),
                shape: BoxShape.rectangle,
              ),
            ),
          ],
        ),
      ),
    );
  }

}