import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_database/firebase_database.dart';
import 'dart:io';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:sportify/models/createEvent/event.dart';
import 'package:sqflite/sqflite.dart';
import '../../components/dialogBox.dart';
import '../../models/groups/group.dart';
import '../app_controller.dart';
import 'package:path_provider/path_provider.dart';

import '../notifications/notification_controller.dart';

class GroupController {
  AppController appController = AppController();
  NotificationController notificationController = NotificationController();

  GroupController();

  late Reference _userStorageRef =
  FirebaseStorage.instance.ref().child('groups');

  Future<String> uploadImage(File imageFile) async {
    final imageRef = _userStorageRef.child(FirebaseAuth.instance.currentUser!.uid);
    await imageRef.putFile(imageFile);
    final String imageUrl = await imageRef.getDownloadURL();
    return imageUrl;
  }

  Group fromDB(Map<String, dynamic> map) {
    return Group(
      name: map['name'],
      sports: (map['sports'] as String).split(','),
      description: map['description'],
      image: map['image'],
      owner: map['owner'],
      members: (map['members'] as String).split(','),
    );
  }

  void uploadGroup(int id, Database database) async {
    var connectivityResult = await appController.getConnectionStatus();
    var startTime = DateTime.now();
    while (connectivityResult == ConnectivityResult.none) {
      if (DateTime.now().difference(startTime).inMinutes > 3) {
        await database.delete('temporal_groups');
        await database.close();
        notificationController.showNotification(
            'Group not created', 'Time limit exceeded');
        return;
      }
      await Future.delayed(Duration(seconds: 1));
      connectivityResult = await appController.getConnectionStatus();
    }
    final databaseReference = FirebaseDatabase.instance.ref();
    final result = await database.query(
      'temporal_groups',
      where: 'id = ?',
      whereArgs: [id],
    );
    Group groupUpload = fromDB(result.first);
    groupUpload.image = await uploadImage(File(groupUpload.image!));
    databaseReference.child('groups').push().set(groupUpload.toJson());
    notificationController.showNotification(
        'Group created', 'Group created successfully');
    await database.delete('temporal_groups');
    await database.close();
  }
}