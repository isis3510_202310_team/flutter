import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';

import '../models/app_model.dart';

class AppController {

  final AppModel model = AppModel();

  Future<void> initializeApp() async {
    // Load fonts
    final fontLoader = FontLoader('Roboto');
    fontLoader.addFont(rootBundle.load('assets/fonts/Roboto-Regular.ttf'));
    fontLoader.addFont(rootBundle.load('assets/fonts/Roboto-Bold.ttf'));
    await fontLoader.load();
  }

  Future<String?> getImageUrl() async {
    return model.getImageUrl();
  }

  Future<String?> getAuthStrategy() async {
    return model.getAuthStrategy();
  }

  Future<void> saveImageUrl(String imageUrl) async {
    return model.saveImageUrl(imageUrl);
  }

  Future<void> saveAuthStrategy(String authStrategy) async {
    return model.saveAuthStrategy(authStrategy);
  }

  Future<Image?> getImageFromCache(String imageUrl) async {
    return model.getImageFromCache(imageUrl);
  }

  Future<Image> getImageFromFirebaseStorage() async {
    return model.getImageFromFirebaseStorage();
  }

  Future<void> saveFormInfoInCache(String formInfo) async {
    return model.saveFormInfoInCache(formInfo);
  }

  Future<String?> getFormInfoFromCache() async {
    return model.getFormInfoFromCache();
  }

  Future<void> deleteFormInfoInCache() async {
    return model.deleteFormInfoInCache();
  }

   Future<ConnectivityResult> getConnectionStatus() async {
     final connectivityResult = await Connectivity().checkConnectivity();
     return connectivityResult;
   }
}