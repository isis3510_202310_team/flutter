import 'package:sportify/models/user_model.dart';

import '../models/sportify_user.dart';

class UserController {

  UserModel model = UserModel();

  Future<SportifyUser?> getUser() async {
    return model.getUser();
  }
}