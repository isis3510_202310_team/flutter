import 'dart:io';

import '../../models/auth/register_model.dart';
import '../../models/authentication/strategies/auth_strategy.dart';
import '../../models/sportify_user.dart';

class RegisterController {
  final RegisterModel model;

  RegisterController({required this.model});

  Future<void> signup(String email, String password) async {
    await model.signup(email, password);
  }

  void setAuthStrategy(AuthStrategy strategy) {
    model.setAuthStrategy(strategy);
  }

  Future<String> uploadAvatar(File imageFile) async {
    return model.uploadAvatar(imageFile);
  }

  void save(SportifyUser user) {
    model.save(user);
  }
}