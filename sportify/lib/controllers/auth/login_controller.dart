import '../../models/authentication/strategies/auth_strategy.dart';
import '../../models/auth/login_model.dart';

class LoginController {
  final LoginModel model;

  LoginController({required this.model});

  Future<void> login(String email, String password) async {
    await model.login(email, password);
  }

  void setAuthStrategy(AuthStrategy strategy) {
    model.setAuthStrategy(strategy);
  }
}
