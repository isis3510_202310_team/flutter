import 'package:google_maps_flutter/google_maps_flutter.dart';
import '../../models/googleAPI/locationModel.dart';
class LocationController {
  final LocationModel locationModel;

  LocationController(this.locationModel);

  void onMapCreated(GoogleMapController controller) {
    locationModel.onMapCreated(controller);
  }

  void onPlacePicked(result) {
    locationModel.onPlacePicked(result);
  }

  void onMarkerTapped(LatLng location) {
    locationModel.onMarkerTapped(location);
  }

  Future<String> getLocationName(double latitude, double longitude) async {
    return await locationModel.getLocationName(latitude, longitude);
  }

}
