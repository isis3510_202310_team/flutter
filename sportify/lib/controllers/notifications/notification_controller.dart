import 'package:sportify/models/notifications/notifications.model.dart';

class NotificationController {
  final NotificationModel model = NotificationModel();

  Future<void> showNotification(String? title, String? body) async {
    return await model.showNotification(title, body);
  }

}