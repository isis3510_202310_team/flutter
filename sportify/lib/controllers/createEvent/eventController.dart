import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_database/firebase_database.dart';
import 'dart:io';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:sportify/models/createEvent/event.dart';
import 'package:sqflite/sqflite.dart';
import '../../components/dialogBox.dart';
import '../app_controller.dart';
import 'package:path_provider/path_provider.dart';

import '../notifications/notification_controller.dart';

class EventController {
  AppController appController = AppController();
  NotificationController notificationController = NotificationController();

  EventController();

  late Reference _userStorageRef =
      FirebaseStorage.instance.ref().child('events');

  Future<String> uploadImage(File imageFile) async {
    final imageRef = _userStorageRef.child(FirebaseAuth.instance.currentUser!.uid);
    await imageRef.putFile(imageFile);
    final String imageUrl = await imageRef.getDownloadURL();
    return imageUrl;
  }

  Event fromDB(Map<String, dynamic> map) {
    return Event(
      idUser: map['idUser'],
      name: map['name'],
      address: map['address'],
      sports: (map['sports'] as String).split(','), // convertir la cadena en una lista
      date: map['date'],
      startTime: map['startTime'],
      endTime: map['endTime'],
      description: map['description'],
      image: map['image'],
    );
  }

  void uploadEvent(int id, Database database) async {
    var connectivityResult = await appController.getConnectionStatus();
    var startTime = DateTime.now();
    while (connectivityResult == ConnectivityResult.none) {
      if (DateTime.now().difference(startTime).inMinutes > 3) {
        await database.delete('temporal_events');
        await database.close();
        notificationController.showNotification(
            'Event not created', 'Time limit exceeded');
        return;
      }
      await Future.delayed(Duration(seconds: 1));
      connectivityResult = await appController.getConnectionStatus();
    }
    final databaseReference = FirebaseDatabase.instance.ref();
    final result = await database.query(
      'temporal_events',
      where: 'id = ?',
      whereArgs: [id],
    );
    Event eventUpload = fromDB(result.first);
    if (eventUpload.image != ""){
      eventUpload.image = await uploadImage(File(eventUpload.image!));
    }else {
      eventUpload.image = "";
    }
    databaseReference.child('events').push().set(eventUpload.toJson());
    notificationController.showNotification(
        'Event created', 'Your event has been created');
    await database.delete('temporal_events');
    await database.close();
  }

}
