import '../../models/createEvent/sports.dart';

class SportsController {

  final ListSports listSports;

  SportsController(this.listSports);

  List<Sport> getSports() {
    return listSports.getSports();
  }

  List<Map<String, String>> getSportsMap() {
    return listSports.getSportsMap();
  }

}