import 'package:flutter/material.dart';

class SportifyColors {
  static const Color primary = Color(0xFF6750A4);
  static const Color secondary = Color(0xFFD0BCFF);
  static const Color tertiary = Color(0xFFD6CEEC);
  static const Color quaternary = Color(0xFFE8E6F3);
  static const Color quinary = Color(0xFFEDECF5);
  static const Color background = Color(0xFFF5F5F5);
  static const Color text = Color(0xFF282828);
}