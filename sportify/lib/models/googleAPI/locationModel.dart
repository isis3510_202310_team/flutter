import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'LocationGoogleServiceAdapter.dart';

class LocationModel implements LocationServiceAdapter {
  late GoogleMapController mapController;
  late Set<Marker> markers = {};
  late String address;
  late LatLng? selectedLocation= null;
  late LatLng initialLocation = LatLng(4.602413, -74.066099);
  final String apiKey = 'AIzaSyCX_ohCLf15hhhFNDv05X5LyXAQvk0s_B8';


  @override
  void onMapCreated(GoogleMapController controller) {
    mapController = controller;
  }

  @override
  void onPlacePicked(result) {
    if (result != null &&
        result.geometry != null &&
        result.geometry!.location != null) {
      final location = LatLng(
        result.geometry!.location!.lat,
        result.geometry!.location!.lng,
      );
      markers.clear();
      markers.add(Marker(
        markerId: MarkerId('event_location'),
        position: location,
      ));
      selectedLocation = location;
    }
  }

  @override
  void onMarkerTapped(LatLng location) {
    markers.clear();
    markers.add(Marker(
      markerId: MarkerId('event_location'),
      position: location,
    ));
  }

  void changeAdress(String newAddress){
    address = newAddress;
  }

  @override
  Future<String> getLocationName(double latitude, double longitude) async {
    final api = apiKey;
    final url =
        'https://maps.googleapis.com/maps/api/geocode/json?latlng=$latitude,$longitude&key=$api';
    final response = await http.get(Uri.parse(url));
    if (response.statusCode == 200) {
      final decoded = json.decode(response.body);
      final address = decoded['results'][0]['formatted_address'];
      changeAdress(address);
      return address;
    } else {
      throw Exception('Error al obtener la ubicación.');
    }
  }

}
