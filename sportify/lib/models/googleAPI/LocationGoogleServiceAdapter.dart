import 'package:google_maps_flutter/google_maps_flutter.dart';

abstract class LocationServiceAdapter {
  Future<String> getLocationName(double latitude, double longitude);
  void onMapCreated(GoogleMapController controller);
  void onPlacePicked(result);
  void onMarkerTapped(LatLng location);
}