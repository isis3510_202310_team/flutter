import '../authentication/strategies/auth_strategy.dart';
import '../authentication/strategies/email_password_auth.dart';
import '../authentication/strategies/google_auth.dart';
import '../authentication/auth_manager.dart';

import 'package:flutter_cache_manager/flutter_cache_manager.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:firebase_database/firebase_database.dart';

class LoginModel {
  final EmailPasswordAuth emailPasswordAuth = EmailPasswordAuth();
  final GoogleAuth googleAuth = GoogleAuth();
  late AuthManager authManager;

  //StorageReference instance
  late Reference _userStorageRef;

  //DatabaseReference instance
  late DatabaseReference _userRef;

  LoginModel() {
    authManager = AuthManager(emailPasswordAuth);
    _userRef = FirebaseDatabase.instance.ref().child('users');
    _userStorageRef = FirebaseStorage.instance.ref().child('avatars');
  }

  Future<User?> login(String email, String password) async {
    User? user = await authManager.login(email, password);
    final userRef = _userRef.child((user?.uid)!);
    userRef.onValue.listen((DatabaseEvent event) {

      final data = event.snapshot.value;
      final imageUrl = findAvatarLink(data.toString());
      loadInCache(imageUrl);
      saveImageUrl(imageUrl);
    });
  }

  String findAvatarLink(String dataString) {
    var pattern = RegExp(r'avatar:\s*(\S+)');
    var match = pattern.firstMatch(dataString);

    if(match?.group(1) == null || match?.group(1) == '') {
      pattern = RegExp(r'avatarUrl:\s*(\S+)');
      match = pattern.firstMatch(dataString);
    }
    return match?.group(1) ?? '';
  }

  void setAuthStrategy(AuthStrategy strategy) {
    authManager.setStrategy(strategy);
  }

  Future<void> loadInCache (String imageUrl) async {
    final cacheManager = DefaultCacheManager();
    await cacheManager.downloadFile(imageUrl);
  }

  Future<void> saveImageUrl(String imageUrl) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString('imageUrl', imageUrl);
  }
}
