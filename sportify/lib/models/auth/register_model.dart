import 'dart:io';

import 'package:firebase_auth/firebase_auth.dart';

import '../authentication/strategies/email_password_auth.dart';
import '../authentication/strategies/google_auth.dart';
import '../authentication/auth_manager.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:firebase_database/firebase_database.dart';
import '../authentication/strategies/auth_strategy.dart';

import 'package:path/path.dart' as Path;
import '../sportify_user.dart';

import 'package:flutter_cache_manager/flutter_cache_manager.dart';
import 'package:shared_preferences/shared_preferences.dart';

class RegisterModel {
  EmailPasswordAuth emailPasswordAuth = EmailPasswordAuth();
  GoogleAuth googleAuth = GoogleAuth();

  //AuthManager instance
  late AuthManager authManager;

  //DatabaseReference instance
  late DatabaseReference _userRef;

  //StorageReference instance
  late Reference _userStorageRef;

  RegisterModel() {
    authManager = AuthManager(emailPasswordAuth);
    _userRef = FirebaseDatabase.instance.ref().child('users');
    _userStorageRef = FirebaseStorage.instance.ref().child('avatars');
  }

  Future<void> signup(String email, String password) async {
    await authManager.signup(email, password);
  }

  void setAuthStrategy(AuthStrategy strategy) {
    authManager.setStrategy(strategy);
  }

  Future<String> uploadAvatar(File imageFile) async {
    final avatarRef = _userStorageRef.child(FirebaseAuth.instance.currentUser!.uid);
    await avatarRef.putFile(imageFile);
    final String imageUrl = await avatarRef.getDownloadURL();

    loadInCache(imageUrl);
    saveImageUrl(imageUrl);
    return imageUrl;
  }

  void save(SportifyUser user) {
    final userRef = _userRef.child(FirebaseAuth.instance.currentUser!.uid);
    userRef.set({
      'fullName': user.fullName,
      'birthDate': user.birthDate,
      'bio': user.bio,
      'phone': user.phone,
      'email': user.email,
      'avatarUrl': user.avatarUrl,
    });
  }

  Future<void> loadInCache (String imageUrl) async {
    final cacheManager = DefaultCacheManager();
    await cacheManager.downloadFile(imageUrl);
  }

  Future<void> saveImageUrl(String imageUrl) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString('imageUrl', imageUrl);
  }
}