import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'auth_exception.dart';
import 'package:http/http.dart' as http;
import 'dart:typed_data';

import 'auth_strategy.dart';


class GoogleAuth implements AuthStrategy {

  final FirebaseAuth _auth = FirebaseAuth.instance;
  final GoogleSignIn googleSignIn = GoogleSignIn();
  final DatabaseReference _userRef = FirebaseDatabase.instance.ref().child('users');
  final Reference _userStorageRef = FirebaseStorage.instance.ref().child('avatars');

  @override
  Future<User?> login(String email, String password) async {

    try {
      GoogleSignInAccount? gUser = await googleSignIn.signIn();
      GoogleSignInAuthentication gAuth = await gUser!.authentication;
      AuthCredential credential = GoogleAuthProvider.credential(
        accessToken: gAuth.accessToken,
        idToken: gAuth.idToken,
      );
      UserCredential userCredential = await _auth.signInWithCredential(credential);

      User? user = userCredential.user;

      if(user != null){
        try{
          final currentUser = FirebaseAuth.instance.currentUser;
          final userId = currentUser?.uid;

          //Get photo
          String? photoUrl = gUser?.photoUrl;
          if(photoUrl != null){
            http.Response response = await http.get(Uri.parse(photoUrl!));
            Uint8List imageData = response.bodyBytes;
            //var imageFile = File(userId!);
            //await imageFile.writeAsBytes(imageData);

            //Upload photo to storage
            final avatarRef = _userStorageRef.child(userId!);
            //await avatarRef.putFile(imageFile!);
            await avatarRef.putData(imageData);

            final String imageUrl = await avatarRef.getDownloadURL();
            final userRef = _userRef.child(userId!);

            userRef.set({
              'avatar': imageUrl,
              'bio': '',
              'birthDate': '',
              'email': gUser.email,
              'fullName': gUser.displayName,
              'phone': '',
            });
          }
          else {
            final userRef = _userRef.child(userId!);

            userRef.set({
              'avatar': '',
              'bio': '',
              'birthDate': '',
              'email': gUser.email,
              'fullName': gUser.displayName,
              'phone': '',
            });
          }

          return user;

        } catch (e) {
          throw AuthException(e.toString());
        }

      }
    } on Exception catch (e) {
      print(e);
      throw AuthException(e.toString());
    }
  }

  @override
  Future<User?> signup(String email, String password) async {

  }

  @override
  Future<void> logout() async {
    await FirebaseAuth.instance.signOut();
    await googleSignIn.signOut();
  }
}