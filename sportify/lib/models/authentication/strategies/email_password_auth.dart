import 'package:firebase_auth/firebase_auth.dart';
import 'auth_exception.dart';
import 'auth_strategy.dart';

class EmailPasswordAuth implements AuthStrategy {
  @override
  Future<User?> login(String email, String password) async {
    try{
      UserCredential userCredential = await FirebaseAuth.instance
          .signInWithEmailAndPassword(email: email, password: password);
      return userCredential.user;
    } on FirebaseAuthException catch (e) {
      //Wrong email!
      if (e.code == 'user-not-found') {
        throw AuthException('user-not-found');
      }

      else if (e.code == 'wrong-password') {
        throw AuthException('wrong-password');
      }

      else {
        throw AuthException(e.toString());
      }

      //Wrong password!
    }
  }
  @override
  Future<User?> signup(String email, String password) async {
    try {
      UserCredential userCredential = await FirebaseAuth.instance
          .createUserWithEmailAndPassword(email: email, password: password);
      return userCredential.user;
    } on FirebaseAuthException catch (e) {
      if (e.code == 'email-already-in-use') {
        throw AuthException('email-already-in-use');
      }

      else if (e.code == 'invalid-email') {
        throw AuthException('invalid-email');
      }

      else if (e.code == 'weak-password') {
        throw AuthException('weak-password');
      }

      else if (e.code == 'unknown') {
        throw AuthException('unknown');
      }
    }
  }


  @override
  Future<void> logout() async {
    FirebaseAuth.instance.signOut();
  }
}