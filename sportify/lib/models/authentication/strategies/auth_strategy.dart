import 'package:firebase_auth/firebase_auth.dart';

abstract class AuthStrategy {
  Future<User?> login(String email, String password);
  Future<User?> signup(String email, String password);
  Future<void> logout();
}