import 'package:firebase_auth/firebase_auth.dart';
import 'strategies/auth_strategy.dart';

class AuthManager {
  AuthStrategy _authStrategy;

  AuthManager(this._authStrategy);

  void setStrategy(AuthStrategy authStrategy) {
    _authStrategy = authStrategy;
  }

  Future<User?> login(String email, String password) {
    return _authStrategy.login(email, password);
  }

  Future<User?> signup(String email, String password) {
    return _authStrategy.signup(email, password);
  }

  Future<void> logout() {
    return _authStrategy.logout();
  }
}