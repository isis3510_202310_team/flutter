import 'dart:convert';
import 'dart:typed_data';
import 'dart:io';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter_image/flutter_image.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_cache_manager/flutter_cache_manager.dart';

import 'package:flutter/services.dart';
import 'package:firebase_core/firebase_core.dart';
import '../firebase_options.dart';
import 'package:shared_preferences/shared_preferences.dart';


class AppModel {

  //StorageReference instance
  late Reference _userStorageRef;
  late String? userId;

  AppModel() {
    _userStorageRef = FirebaseStorage.instance.ref().child('avatars');
    final currentUser = FirebaseAuth.instance.currentUser;
    userId = currentUser?.uid;
  }

  Future<void> initializeApp() async {
    // Load fonts
    final fontLoader = FontLoader('Roboto');
    fontLoader.addFont(rootBundle.load('assets/fonts/Roboto-Regular.ttf'));
    fontLoader.addFont(rootBundle.load('assets/fonts/Roboto-Bold.ttf'));
    await fontLoader.load();

    // Initialize Firebase
    await Firebase.initializeApp(
      options: DefaultFirebaseOptions.currentPlatform,
    );
  }

  Future<String?> getImageUrl() async {
    final avatarRef = _userStorageRef.child(FirebaseAuth.instance.currentUser!.uid);
    final String imageUrl = await avatarRef.getDownloadURL();

    return imageUrl;
  }

  Future<String?> getAuthStrategy() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString('authStrategy');
  }

  Future<void> saveImageUrl(String imageUrl) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString('imageUrl', imageUrl);
  }

  Future<void> saveAuthStrategy(String authStrategy) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString('authStrategy', authStrategy);
  }

  Future<Image?> getImageFromCache(String imageUrl) async {
    final cacheManager = DefaultCacheManager();
    final fileStream = cacheManager.getFileFromCache(imageUrl);
    final file = await fileStream;

    if (file != null && file.file != null) {
      final image = Image.file(file.file! as File);
      return image;
    } else {
      return null;
    }
  }


  Future<Image> getImageFromFirebaseStorage() async {

    final storageRef = _userStorageRef.child(userId!);
    final url = await storageRef.getDownloadURL();

    final image = Image(
      image: NetworkImageWithRetry(url),
    );

    return image;
  }


  Future<void> saveFormInfoInCache(String formInfo) async {
    final cacheManager = DefaultCacheManager();
    List<int> bytes = utf8.encode(formInfo);

    Uint8List uint8list = Uint8List.fromList(bytes);

    await cacheManager.putFile(
      'formInfo.txt',
      uint8list,
      maxAge: const Duration(days: 1),
      fileExtension: '.txt',
    );
  }

  Future<String?> getFormInfoFromCache() async {
    final cacheManager = DefaultCacheManager();
    FileInfo? fileInfo = await cacheManager.getFileFromCache('formInfo.txt');

    if (fileInfo != null && fileInfo.file.existsSync()) {
      List<int> bytes = await fileInfo.file.readAsBytes();
      String formInfo = utf8.decode(bytes);
      return formInfo;
    } else {
      return null;
    }
  }

  Future<void> deleteFormInfoInCache() async {
    final cacheManager = DefaultCacheManager();
    await cacheManager.removeFile('formInfo.txt');
  }
}