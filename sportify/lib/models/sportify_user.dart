class SportifyUser {
  late String fullName;
  late String birthDate;
  late String bio;
  late int phone;
  late String email;
  late String password;
  late String avatarUrl;

  @override
  String toString() {
    return '{fullName: $fullName, birthDate: $birthDate, bio: $bio, phone: $phone, email: $email, password: $password, avatarUrl: $avatarUrl}';
  }

  static SportifyUser fromString(String string) {
    List<String> parts = string.split(',');
    SportifyUser user = SportifyUser();
    for (String part in parts) {
      List<String> keyValue = part.split(':');
      String key = keyValue[0].trim().replaceAll(RegExp(r'^{|}$'), '');
      String value = keyValue[1].trim().replaceAll(RegExp(r'^{|}$'), '');
      if (key == 'fullName') {
        user.fullName = value;
      } else if (key == 'birthDate') {
        user.birthDate = value;
      } else if (key == 'bio') {
        user.bio = value;
      } else if (key == 'phone') {
        user.phone = int.parse(value);
      } else if (key == 'email') {
        user.email = value;
      } else if (key == 'password') {
        user.password = value;
      } else if (key == 'avatarUrl') {
        user.avatarUrl = value;
      }
    }

    return user;
  }
}

