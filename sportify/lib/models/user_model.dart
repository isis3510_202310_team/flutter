import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:sportify/models/sportify_user.dart';

class UserModel {

  late DatabaseReference _userRef;
  late String? userId;


  UserModel() {
    _userRef = FirebaseDatabase.instance.ref().child('users');
    final currentUser = FirebaseAuth.instance.currentUser;
    userId = currentUser?.uid;
  }

  Future<SportifyUser?> getUser() async {
    DatabaseEvent event = await _userRef.child(userId!).once();
    if (event.snapshot.value != null) {
      return SportifyUser.fromString(event.snapshot.value.toString());
    } else {
      return null;
    }
  }

}