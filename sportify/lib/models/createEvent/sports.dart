 class Sport {
  String name;
  String emoji;
  Sport({required this.name, required this.emoji});
}

class ListSports {
  List<Sport> sports = [
    Sport(name: 'Basketball', emoji: '🏀'),
    Sport(name: 'Boxing', emoji: '🥊'),
    Sport(name: 'Football', emoji: '⚽'),
    Sport(name: 'Golf', emoji: '🏌️‍♂️'),
    Sport(name: 'Ping Pong', emoji: '🏓'),
    Sport(name: 'Rugby', emoji: '🏉'),
    Sport(name: 'Tennis', emoji: '🎾'),
    Sport(name: 'Volleyball', emoji: '🏐'),
  ];

  List<Sport> getSports() {
    return sports;
  }

  List<Map<String, String>> getSportsMap() {
    List<Map<String, String>> sportsMap = [];
    sports.forEach((sport) {
      sportsMap.add({
        'name': sport.name,
        'emoji': sport.emoji,
      });
    });
    return sportsMap;
  }
}


