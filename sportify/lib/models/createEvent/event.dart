class Event {
  String? idUser;
  String? name;
  String? address;
  List<String>? sports;
  String? date;
  String? startTime;
  String? endTime;
  String? description;
  String? image;

  Event({
    this.idUser,
    this.name,
    this.address,
    this.sports,
    this.date,
    this.startTime,
    this.endTime,
    this.description,
    this.image,
  });

  Map<String, dynamic> toJson() {
    return {
      "idUser": idUser,
      "name": name,
      "address": address,
      "sports": sports,
      "date": date,
      "startTime": startTime,
      "endTime": endTime,
      "description": description,
      "image": image,
    };
  }

  Map<String, dynamic> toDB() {
    return {
      "idUser": idUser,
      "name": name,
      "address": address,
      "sports": sports?.join(','), // convertir la lista en una cadena separada por comas
      "date": date,
      "startTime": startTime,
      "endTime": endTime,
      "description": description,
      "image": image,
    };
  }

}