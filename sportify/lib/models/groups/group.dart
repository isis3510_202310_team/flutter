class Group {
  String? owner;
  String? image;
  String? name;
  List<String>? sports;
  String? description;
  List<String>? members;

  Group({
    this.owner,
    this.image,
    this.name,
    this.sports,
    this.description,
    this.members,
  });

  Map<String, dynamic> toJson() {
    return {
      "idAdmin": owner,
      "image": image,
      "name": name,
      "sports": sports,
      "description": description,
      "members": members,
    };
  }

  Map<String, dynamic> toDB() {
    return {
      "owner": owner,
      "image": image,
      "name": name,
      "sports": sports?.join(','),
      "description": description,
      "members": members?.join(','),
    };
  }

}