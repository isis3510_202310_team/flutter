import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';

class NotificationModel {
  FirebaseMessaging messaging = FirebaseMessaging.instance;
  FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
  FlutterLocalNotificationsPlugin();

  Future<String> sendPushNotification(String title, String body) async {
    final url = Uri.parse('https://fcm.googleapis.com/fcm/send');
    String? token = await FirebaseMessaging.instance.getToken();
    // Estructura del mensaje de notificación
    final message = {
      'notification': {'title': title, 'body': body},
      'priority': 'high',
      'to': token,
    };

    final headers = {
      'Content-Type': 'application/json',
      'Authorization': 'key=YOUR_SERVER_KEY', // Reemplaza con tu clave de servidor de Firebase
    };

    // Realizar la solicitud HTTP POST a la API de FCM
    final response = await http.post(url, headers: headers, body: jsonEncode(message));

    Future<void> _firebaseMessagingBackgroundHandler(RemoteMessage message) async {
      // Cuando se recibe un mensaje de FCM en segundo plano
      showNotification(message.notification?.title, message.notification?.body);
    }
    FirebaseMessaging.onBackgroundMessage(_firebaseMessagingBackgroundHandler);
    FirebaseMessaging.onMessage.listen((RemoteMessage message) {
      // Cuando se recibe un mensaje de FCM en primer plano
      showNotification(message.notification?.title, message.notification?.body);
    });
    // Inicializar FlutterLocalNotificationsPlugin
    const AndroidInitializationSettings initializationSettingsAndroid =
    AndroidInitializationSettings('@mipmap/ic_launcher');
    final InitializationSettings initializationSettings = InitializationSettings(
      android: initializationSettingsAndroid,
    );
    await flutterLocalNotificationsPlugin.initialize(initializationSettings);

    if (response.statusCode == 200) {
      print('Notificación push enviada correctamente.');
      return 'Notificación push enviada correctamente.';
    } else {
      print('Error al enviar la notificación push. Código de respuesta: ${response.statusCode}');
      return 'Error al enviar la notificación push. Código de respuesta: ${response.statusCode}';
    }
  }

  Future<void> showNotification(String? title, String? body) async {

    const AndroidNotificationDetails androidPlatformChannelSpecifics =
    AndroidNotificationDetails(
      'channel_id',
      'channel_name',
      importance: Importance.max,
      priority: Priority.high,
      showWhen: false,
    );
    const NotificationDetails platformChannelSpecifics =
    NotificationDetails(android: androidPlatformChannelSpecifics);

    await flutterLocalNotificationsPlugin.show(
      0, // ID de la notificación
      title, // Título de la notificación
      body, // Cuerpo de la notificación
      platformChannelSpecifics,
    );
  }
}